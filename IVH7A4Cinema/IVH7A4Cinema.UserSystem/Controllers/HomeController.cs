﻿using System.Linq;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.UserSystem.Controllers
{
    public class HomeController : Controller
    {
        private IUserRepository userRepository;

        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(User user)
        {
            int result = UserValidation(user.UserName, user.Password);

            if (result == 1)
            {
                ViewBag.Error = "De ingevoerde gegevens zijn ongeldig";
                //return View("~/Home/Index");
                return View("AfterLogin");
            } else
            {
                return View("Index");
            }

            /*
            // this action is for handle post (login)
            if (ModelState.IsValid) // this will check validity
            {
                User dc = UserRepository.Users;
                var v = dc.Users.Where(a => a.Username.Equals(u.Username) && a.Password.Equals(u.Password)).FirstOrDefault();
                if (v != null)
                {
                    Session["LoggedUserID"] = v.UserID.ToString();
                    Session["LoggedUserFullName"] = v.FullName.ToString();
                    return RedirectToAction("AfterLogin");
                }
            }
            return View(u);*/
        }

        public int UserValidation(string userName, string password)
        {
            int result = 0;
            User user = userRepository.Users.Where(a => a.UserName == userName).FirstOrDefault();

            if (user != null)
            {
                if (user.UserName == userName)// && user.Password == password)
                {
                    result = 1;
                }
            } else{
                result = 0;
            }
            return result;
        }

        /*public ActionResult AfterLogin()
        {
            if (Session["LoggedUserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("");
            }
        }*/
    }
}