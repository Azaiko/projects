﻿using System.ComponentModel.DataAnnotations;

namespace IVH7A4Cinema.Domain.Entities
{
    public class Room
    {
        [Key]
        public int RoomNumber { get; set; }
        public string RoomLayout { get; set; }
        public string Type { get; set; }
        public bool WheelChair { get; set; }
        public int TotalSeats { get; set; }
    }
}
