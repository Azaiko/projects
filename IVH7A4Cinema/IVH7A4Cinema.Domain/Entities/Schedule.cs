﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IVH7A4Cinema.Domain.Entities
{
    public class Schedule
    {
        [Key]
        public int ScheduleID { get; set; }
        [ForeignKey("Room")]
        public int RoomID { get; set; }
        public virtual Room Room { get; set; }
        public int FilmID { get; set; }
        public virtual Film Film { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }
    }
}
