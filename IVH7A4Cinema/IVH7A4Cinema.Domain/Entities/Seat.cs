﻿namespace IVH7A4Cinema.Domain.Entities
{
    public class Seat
    {
        public int Row { get; set; }
        public int Number { get; set; }
    }
}
