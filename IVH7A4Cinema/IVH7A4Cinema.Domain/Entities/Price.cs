﻿using System.ComponentModel.DataAnnotations;

namespace IVH7A4Cinema.Domain.Entities
{
    public class Price
    {
        [Key]
        public int PriceID { get; set; }
        public decimal TicketPrice { get; set; }
        public decimal TicketPriceLong { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public decimal DiscountChild { get; set; }
        public decimal DiscountStudent { get; set; }
        public decimal DiscountSenior { get; set; }
        public decimal Addition3D { get; set; }
    }
}
