﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVH7A4Cinema.Domain.Entities
{
    public class Reservation
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int TicketID { get; set; }
        [Key]
        [Column(Order = 1)]
        [Range(0, int.MaxValue, ErrorMessage = "Voer een geldig ID in")]
        [Required(ErrorMessage = "Voer een geldig ID in")]
        public int ReservationID { get; set; }
        public int CustomerID { get; set; }
        public int RowNumber { get; set; }
        public int SeatNumber { get; set; }
        public int ScheduleID { get; set; }
        public virtual Schedule Schedule { get; set; }
        public bool Paid { get; set; }
        public bool Printed { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
