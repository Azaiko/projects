﻿using System;

namespace IVH7A4Cinema.Domain.Entities
{
    public class Film
    {
        public int FilmID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public string Poster { get; set; }
        public string Language { get; set; }
        public int length { get; set; }
        public string Type { get; set; }
        public string Trailer { get; set; }
        public string Age { get; set; }
        public string SubTitles { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string IMDB { get; set; }

    }
}
