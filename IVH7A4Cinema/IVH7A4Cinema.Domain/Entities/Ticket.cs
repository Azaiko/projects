﻿namespace IVH7A4Cinema.Domain.Entities
{
    public class Ticket
    {
        public int PriceID { get; set; }
        public virtual Price Price { get; set; }
        public int Amount { get; set; }
    }
}
