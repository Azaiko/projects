﻿using IVH7A4Cinema.Domain.Abstract;
using System.Collections.Generic;
using IVH7A4Cinema.Domain.Entities;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.Domain.Concrete
{
    [ExcludeFromCodeCoverage]
    public class EFPriceRepository : IPriceRepository
    {
        private EFDbContext contextDb = new EFDbContext();

        public IEnumerable<Price> Prices
        {
            get { return contextDb.Price; }
        }
    }
}
