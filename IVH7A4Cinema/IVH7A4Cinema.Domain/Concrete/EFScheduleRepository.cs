﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.Domain.Concrete
{
    [ExcludeFromCodeCoverage]
    public class EFScheduleRepository : IScheduleRepository
    {
        private EFDbContext contextDb = new EFDbContext();

        public IEnumerable<Schedule> Schedules
        {
            get { return contextDb.Schedule; }
        }
    }
}
