﻿using System;
using System.Collections.Generic;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.Domain.Concrete
{
    [ExcludeFromCodeCoverage]
    public class EFUserRepository : IUserRepository
    {
        private EFDbContext contextDb = new EFDbContext();

        public IEnumerable<User> Users
        {
            get { return contextDb.User; }
        }
    }
}
