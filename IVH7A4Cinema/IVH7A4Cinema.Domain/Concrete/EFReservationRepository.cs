﻿using System;
using System.Collections.Generic;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.Domain.Concrete
{
    [ExcludeFromCodeCoverage]
    public class EFReservationRepository : IReservationRepository
    {
        private EFDbContext contextDb = new EFDbContext();

        public IEnumerable<Reservation> Reservations
        {
            get { return contextDb.Reservation; }
        }

        public void SaveReservation(Reservation Reservation)
        {
            if(Reservation.TicketID == 0)
            {
                contextDb.Reservation.Add(Reservation);
            }
            else
            {
                Reservation dbEntry = contextDb.Reservation.Find(Reservation.TicketID, Reservation.ReservationID);

                if(dbEntry != null)
                {
                    dbEntry.Paid = Reservation.Paid;
                    dbEntry.Printed = Reservation.Printed;
                }
            }

            contextDb.SaveChanges();
        }
    }
}
