﻿using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.Domain.Migrations;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.Domain.Concrete
{
    [ExcludeFromCodeCoverage]
    public class EFDbContext : DbContext
    {
        public EFDbContext()
        {
            Database.SetInitializer<EFDbContext>(null);
        }
        public DbSet<Film> Film { get; set; }

        public DbSet<Customer> Customer { get; }

        public DbSet<Schedule> Schedule { get; set; }

        public DbSet<Reservation> Reservation { get; set; }

        public DbSet<Room> Room { get; set; }

        public DbSet<Price> Price { get; set; }

        public DbSet<User> User { get; set; }
    }
}
