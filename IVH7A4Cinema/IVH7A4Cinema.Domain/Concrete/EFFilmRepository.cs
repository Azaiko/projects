﻿using System;
using System.Collections.Generic;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.Domain.Concrete
{
    [ExcludeFromCodeCoverage]
    public class EFFilmRepository : IFilmRepository
    {
        private EFDbContext contextDb = new EFDbContext();

        public IEnumerable<Film> Films
        {
            get { return contextDb.Film; }
        }

        public IEnumerable<Schedule> Schedules { get { return contextDb.Schedule; } }
        
    }
}
