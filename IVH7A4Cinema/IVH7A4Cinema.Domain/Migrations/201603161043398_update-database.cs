namespace IVH7A4Cinema.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Films", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Films", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Films", "EndDate");
            DropColumn("dbo.Films", "StartDate");
        }
    }
}
