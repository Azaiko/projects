namespace IVH7A4Cinema.Domain.Migrations
{
    using Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Globalization;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IVH7A4Cinema.Domain.Concrete.EFDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "IVH7A4Cinema.Domain.Concrete.EFDbContext";
        }

        protected override void Seed(IVH7A4Cinema.Domain.Concrete.EFDbContext context)
        {
            context.Film.AddOrUpdate(f => f.FilmID,
                new Film() { FilmID = 1, Name = "Deadpool", Description = "Na een duister experiment genezen zijn verwondingen razendsnel en bedenkt hij het alter ego Deadpool. Gewapend met zijn nieuwe gave en een goed gevoel voor (zwarte) humor gaat Deadpool op zoek naar de man die zijn leven bijna verwoestte", Director = "Tim Miller", Language = "Engels", length = 109, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=ZIM1HydF9UA&oref=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DZIM1HydF9UA", Age = "18+", SubTitles = "Nederlands", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt1431045/" },
                new Film() { FilmID = 2, Name = "London has Fallen", Description = "Wanneer de Engelse premier onder verdachte omstandigheden dood wordt gevonden, gaan alle alarmbellen af. Zijn begrafenis blijkt de ideale plek om een aanslag te plegen en de wereld ligt plots in de handen van slechts drie personen: de president van de Verenigde Staten (Eckhart), zijn meest betrouwbare geheim agent (Butler) en een Engelse MI-6 agent (Charlotte Riley)� ", Director = "Babak Najafi", Language = "Engels", length = 99, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=3AsOdX7NcJs", Age = "16+", SubTitles = "Nederlands", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt3300542/" },
                new Film() { FilmID = 3, Name = "The Revenant", Description = "The Revenant is een episch avontuur van ��n man over overleven en de buitengewone kracht van de menselijke geest. Leonardo DiCaprio heeft voor zijn rol in The Revenant de Oscar gewonnen in de categorie Beste Acteur.I��rritu wint het beeldje in de categorie Beste Regie. Diep in de Amerikaanse wildernis wordt jager Hugh Glass(Leonardo DiCaprio) voor dood achtergelaten door John Fitzgerald(Tom Hardy), een onbetrouwbaar lid van zijn gezelschap.Met alleen zijn wil als wapen, begint Glass aan een meedogenloze missie. Hij moet zich een weg zien te banen door een levensgevaarlijke omgeving,en een strenge winter en onderling strijdende stammen overleven om wraak te kunnen nemen op Fitzgerald. Ge�nspireerd op een waargebeurd verhaal.De film is geregisseerd en deels geschreven door de vermaarde filmmaker en Oscar�winnaar Alejandro Gonz�lez I��rritu(Birdman, Babel).", Director = "Alejando Gonzalez Inarritu", Language = "Engels", length = 156, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=LoebZZ8K5N0", Age = "18+", SubTitles = "Nederlands", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt1663202/" },
                new Film() { FilmID = 4, Name = "Zootropolis", Description = "Zootropolis is een stad als geen ander, een moderne wereld met alleen dieren. De stad bestaat uit verschillende wijken zoals de chique Sahara Square en het ijskoude Tundratown. Het is een mengelmoes van dieren uit verschillende omgevingen die samenleven. Het maakt niet uit wie of wat je bent, van de grootste olifant tot de kleinste spitsmuis. Maar wanneer de optimistische officier Judy Hopps (Ginnifer Goodwin) zich aansluit bij de politie, ontdekt ze dat het niet zo makkelijk is als eerste konijn bij het politiekorps vol grote, stoere dieren. Vastberaden om zichzelf te bewijzen, grijpt ze de kans aan om een zaak op te lossen. Zelfs als dat betekent dat ze moet samenwerken met de welbespraakte, sluwe vos Nick Wilde (Jason Bateman) om het mysterie op te lossen.", Director = "Byron Howard", Language = "Engels", length = 109, Type = "3D", Trailer = "https://www.youtube.com/watch?v=VCDQRy1od30", Age = "6+", SubTitles = "Nederlands", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt2948356/" },
                new Film() { FilmID = 5, Name = "Woezel & Pip Op zoek naar de sloddervos", Description = "Woezel & Pip Op zoek naar de Sloddervos! is een vriendschapsfilm vol kleur, emotie en avontuur. Het is bijna groot feest in de Tovertuin,want de Wijze Varen wordt alweer een fraai jaar ouder.Woezel & Pip krijgen de belangrijke taak om het grote verjaardagscadeau te verstoppen, totdat de grote dag daar is. Tante Perenboom kondigt aan dat er een hele speciale gast komt logeren: het schattige neefje Charlie. Al snel ontdekken Woezel & Pip dat het cadeau en hun spullen zijn verdwenen! Wie heeft dat gedaan ? De Wijze Varen weet het antwoord, maar of de vriendjes daar zo blij mee zijn ? Woezel & Pip, Buurpoes, Molletje, Vlindertje en Charlie gaan in dit spannende avontuur op zoek naar de gevaarlijke indringer.", Director = "Rijk Aardse", Language = "Nederlands", length = 70, Type = "Normaal, Tekenfilm", Trailer = "https://www.youtube.com/watch?v=Z55IMJO2NLk", Age = "Alle leeftijden", SubTitles = "Geen", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt4809396/" },
                new Film() { FilmID = 6, Name = "Dirty Grandpa", Description = "Over een week gaat de brave advocaat Jason (Zac Efron) trouwen met Meredith (Julianne Hough); een ontzettende control freak. Tussen alle trouwdagstress wil zijn opa (Robert De Niro) opeens een lift naar Florida. Maar daar is Spring Break in volle gang en komt het aanstaande huwelijk van Jason plots in gevaar. Want via uitzinnige feesten, bargevechten en een epische karaoke nacht is �vieze� opa vast van plan zijn kleinzoon mee te sleuren in de wildste trip van hun leven!", Director = "Dan Mazer", Language = "Engels", length = 102, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=vOAn83vOZLk", Age = "16+", SubTitles = "Nederlands", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt1860213/" },
                new Film() { FilmID = 7, Name = "Robinson Crusoe", Description = "Papegaai Dinsdag woont met zijn dierenvrienden op een paradijselijk eiland waar het ze aan niks ontbreekt. Toch droomt hij er van om de rest van de wereld te ontdekken. Na een hevige storm spoelt een merkwaardig wezen aan op het eiland: Robinson Crusoe. Dinsdag is dolblij met de nieuwkomer, wie weet is dit zijn kans om van het eiland af te komen en nieuwe plekken te ontdekken! Robinson heeft de dieren hard nodig om te overleven op het eiland en dat is in het begin nog niet makkelijk, want ze spreken geen �mens�.  Langzaam maar zeker leren Robinson en de dieren met elkaar te leven tot op een dag twee gemene katten het eiland willen overnemen. Er ontstaat een spannende strijd waarin Robinson en zijn vrienden zullen ontdekken hoe groot de kracht van vriendschap is.", Director = "Ben Stassen", Language = "Nederlands", length = 91, Type = "Normaal, Animatiefilm", Trailer = "https://www.youtube.com/watch?v=bo0vl4QeD-Q", Age = "6+", SubTitles = "Geen", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt4731008/" },
                new Film() { FilmID = 8, Name = "BUURMAN & BUURMAN - AL 40 JAAR BESTE VRIENDEN!", Description = "Buurman & Buurman bestaan 40 jaar en dat mag gevierd worden! Daarom komen onze doe-het-zelf vrienden met nog nooit eerder in de bioscoop vertoonde afleveringen. Een uur vol inventief geklus met de stemmen van Kees Prins en Siem van LeeuweBuurman & Buurman � al 40 jaar beste vrienden! is een compilatie van de ongekend populaire serie over twee klunzige maar altijd positieve buurmannen.Met vijf gloednieuwe afleveringen en twee klassiekers in HD! Aan de lopende band bedenken de vrienden nieuwe klus-ideee�n om hun levens nog leuker en handiger te maken. Het resultaat liegt er niet om: met hun eigenzinnige geklus is niets in huis veilig en de ravage blijkt enorm. Vol goede moed zien de buurmannen dit als een mooie aanleiding voor een nieuwe klus en lossen ze de mankementen samen weer op!", Director = "Marek Beneses", Language = "Nederlands", length = 56, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=CZrAp3SKD1s", Age = "3+", SubTitles = "Geen", StartDate = DateTime.ParseExact("20/02/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), EndDate = DateTime.ParseExact("20/04/2016 12:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture), IMDB = "http://www.imdb.com/title/tt5320892/" }
                );

            context.Room.AddOrUpdate(ro => ro.RoomNumber,
                new Room()
                {
                    RoomNumber = 1,
                    RoomLayout = " 8| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "7| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
" 6| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
" 5| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
"4| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
" 3| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
" 2| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
" 1| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a",
                    Type = "3D",
                    WheelChair = true,
                    TotalSeats = 120
                },
                new Room()
                {
                    RoomNumber = 2,
                    Type = "3D",
                    WheelChair = true,
                    TotalSeats = 120,
                    RoomLayout = " 8| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "7| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "6| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "5| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "4| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "3 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "2 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "1 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a"
                },
                new Room()
                {
                    RoomNumber = 3,
                    Type = "Normaal",
                    WheelChair = true,
                    TotalSeats = 120,
                    RoomLayout = "8| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "7 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "6 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "5 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "4 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "3 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "2 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a\r\n" +
 "1 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a"
                },
                new Room()
                {
                    RoomNumber = 4,
                    Type = "Normaal",
                    WheelChair = true,
                    TotalSeats = 60,
                    RoomLayout = "6| g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "5| g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "4 | g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "3 | g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "2 | g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "1 | g, g, a, a, a, a, a, g, g, a, a, a, a, a"
                },
                new Room()
                {
                    RoomNumber = 5,
                    Type = "Normaal",
                    WheelChair = false,
                    TotalSeats = 50,
                    RoomLayout = "4| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a, a\r\n" +
 "3 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a, a\r\n" +
 "2 | g, g, g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "1 | g, g, g, g, a, a, a, a, a, g, g, a, a, a, a, a"
                },
                new Room()
                {
                    RoomNumber = 6,
                    Type = "Normaal",
                    WheelChair = false,
                    TotalSeats = 50,
                    RoomLayout = "4| g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a, a\r\n" +
 "3 | g, g, a, a, a, a, a, a, a, g, g, a, a, a, a, a, a, a, a\r\n" +
 "2 | g, g, g, g, a, a, a, a, a, g, g, a, a, a, a, a\r\n" +
 "1 | g, g, g, g, a, a, a, a, a, g, g, a, a, a, a, a"
                }
                );

            context.Price.AddOrUpdate(pr => pr.PriceID,
                new Price() { PriceID = 1, TicketPrice = 8.50M, TicketPriceLong = 9.00M, Name = "Normal", Description = "De normale tickets zijn voor iedereen beschikbaar. Deze kunnen altijd gekozen worden.", DisplayName = "Normale ticket" },
                new Price() { PriceID = 2, TicketPrice = 7.00M, TicketPriceLong = 7.50M, Name = "Child", Description = "Kindertickets zijn alleen beschikbaar voor kinderen tot en met 11 jaar. Wanneer de kinderen ouder zijn dan 11 zijn de tickets ongeldig. De kindertickets zijn geldig tot 18:00 en alleen op Nederlands talige kinderfilms", DisplayName = "Kinderticket" },
                new Price() { PriceID = 3, TicketPrice = 7.00M, TicketPriceLong = 7.50M, Name = "Student", Description = "Studententickets zijn alleen geldig op vertoon van een geldig studentenbewijs. Zonder vertoon van een studentenbewijs bij entree naar de zaal zullen de tickets ongeldig verklaard worden. De studententickets zijn alleen geldig op maandag t/m donderdag", DisplayName = "Studententicket" },
                new Price() { PriceID = 4, TicketPrice = 7.00M, TicketPriceLong = 7.50M, Name = "Senior", Description = "Seniorentickets zijn alleen geldig op vertoon van een seniorenpas. Zonder vertoon van een seniorenpas bij entree naar de zaal kunnen de tickets ongeldig verklaard worden. Seniorentickets zijn alleen geldig op maandag t/m donderdag", DisplayName = "Seniorenticket" }
                );

            context.Schedule.AddOrUpdate(s => s.ScheduleID,
                new Schedule() { ScheduleID = 1, Date = DateTime.Today, RoomID = 1, FilmID = 4 },
                new Schedule() { ScheduleID = 2, Date = DateTime.Today, Time = TimeSpan.Parse("18:00:00"), RoomID = 1, FilmID = 4 },
                new Schedule() { ScheduleID = 3, Date = DateTime.Today, Time = TimeSpan.Parse("18:20:00"), RoomID = 1, FilmID = 4 },
                new Schedule() { ScheduleID = 4, Date = DateTime.Today, Time = TimeSpan.Parse("20:30:00"), RoomID = 2, FilmID = 1 },
                new Schedule() { ScheduleID = 5, Date = DateTime.Today, Time = TimeSpan.Parse("20:00:00"), RoomID = 2, FilmID = 1 },
                new Schedule() { ScheduleID = 6, Date = DateTime.Today, Time = TimeSpan.Parse("22:00:00"), RoomID = 1, FilmID = 4 },
                new Schedule() { ScheduleID = 7, Date = DateTime.Now.Date.AddDays(1), Time = TimeSpan.Parse("18:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 8, Date = DateTime.Now.Date.AddDays(1), Time = TimeSpan.Parse("23:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 9, Date = DateTime.Now.Date.AddDays(1), Time = TimeSpan.Parse("20:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 10, Date = DateTime.Now.Date.AddDays(1), Time = TimeSpan.Parse("18:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 11, Date = DateTime.Now.Date.AddDays(1), Time = TimeSpan.Parse("19:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 12, Date = DateTime.Now.Date.AddDays(2), Time = TimeSpan.Parse("13:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 13, Date = DateTime.Now.Date.AddDays(2), Time = TimeSpan.Parse("19:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 14, Date = DateTime.Now.Date.AddDays(2), Time = TimeSpan.Parse("15:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 15, Date = DateTime.Now.Date.AddDays(2), Time = TimeSpan.Parse("20:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 16, Date = DateTime.Now.Date.AddDays(2), Time = TimeSpan.Parse("16:00:00"), RoomID = 5, FilmID = 5 },
                new Schedule() { ScheduleID = 17, Date = DateTime.Now.Date.AddDays(3), Time = TimeSpan.Parse("23:30:00"), RoomID = 5, FilmID = 5 },
                new Schedule() { ScheduleID = 18, Date = DateTime.Now.Date.AddDays(3), Time = TimeSpan.Parse("19:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 19, Date = DateTime.Now.Date.AddDays(3), Time = TimeSpan.Parse("21:00:00"), RoomID = 4, FilmID = 3 },
                new Schedule() { ScheduleID = 20, Date = DateTime.Now.Date.AddDays(3), Time = TimeSpan.Parse("16:00:00"), RoomID = 5, FilmID = 5 },
                new Schedule() { ScheduleID = 21, Date = DateTime.Now.Date.AddDays(4), Time = TimeSpan.Parse("23:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 22, Date = DateTime.Now.Date.AddDays(4), Time = TimeSpan.Parse("20:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 23, Date = DateTime.Now.Date.AddDays(4), Time = TimeSpan.Parse("19:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 24, Date = DateTime.Now.Date.AddDays(4), Time = TimeSpan.Parse("15:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 25, Date = DateTime.Now.Date.AddDays(4), Time = TimeSpan.Parse("20:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 26, Date = DateTime.Now.Date.AddDays(1), Time = TimeSpan.Parse("18:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 27, Date = DateTime.Now.Date.AddDays(5), Time = TimeSpan.Parse("23:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 28, Date = DateTime.Now.Date.AddDays(5), Time = TimeSpan.Parse("20:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 29, Date = DateTime.Now.Date.AddDays(5), Time = TimeSpan.Parse("18:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 30, Date = DateTime.Now.Date.AddDays(5), Time = TimeSpan.Parse("19:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 31, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("13:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 32, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("19:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 33, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("15:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 34, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("20:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 35, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("16:00:00"), RoomID = 5, FilmID = 5 },
                new Schedule() { ScheduleID = 36, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("23:30:00"), RoomID = 5, FilmID = 5 },
                new Schedule() { ScheduleID = 37, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("19:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 38, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("21:00:00"), RoomID = 4, FilmID = 3 },
                new Schedule() { ScheduleID = 39, Date = DateTime.Now.Date.AddDays(6), Time = TimeSpan.Parse("16:00:00"), RoomID = 5, FilmID = 5 },
                new Schedule() { ScheduleID = 40, Date = DateTime.Now.Date.AddDays(7), Time = TimeSpan.Parse("23:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 41, Date = DateTime.Now.Date.AddDays(7), Time = TimeSpan.Parse("20:00:00"), RoomID = 2, FilmID = 2 },
                new Schedule() { ScheduleID = 42, Date = DateTime.Now.Date.AddDays(7), Time = TimeSpan.Parse("19:00:00"), RoomID = 5, FilmID = 7 },
                new Schedule() { ScheduleID = 43, Date = DateTime.Now.Date.AddDays(7), Time = TimeSpan.Parse("15:00:00"), RoomID = 3, FilmID = 3 },
                new Schedule() { ScheduleID = 44, Date = DateTime.Now.Date.AddDays(7), Time = TimeSpan.Parse("20:00:00"), RoomID = 3, FilmID = 3 }
                );
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
