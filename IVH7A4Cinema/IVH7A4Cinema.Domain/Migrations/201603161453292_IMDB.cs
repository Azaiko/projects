namespace IVH7A4Cinema.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IMDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Films", "IMDB", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Films", "IMDB");
        }
    }
}
