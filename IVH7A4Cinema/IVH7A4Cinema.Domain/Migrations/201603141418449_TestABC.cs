namespace IVH7A4Cinema.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestABC : DbMigration
    {
        public override void Up()
        {

            DropForeignKey("dbo.Reservations", "ScheduleID", "dbo.Schedules");
            DropForeignKey("dbo.Schedules", "RoomID", "dbo.Rooms");
            DropForeignKey("dbo.Schedules", "FilmID", "dbo.Films");
            DropIndex("dbo.Schedules", new[] { "FilmID" });
            DropIndex("dbo.Schedules", new[] { "RoomID" });
            DropIndex("dbo.Reservations", new[] { "ScheduleID" });
            DropTable("dbo.Rooms");
            DropTable("dbo.Schedules");
            DropTable("dbo.Reservations");
            DropTable("dbo.Prices");
            DropTable("dbo.Films");
            DropTable("dbo.Customers");
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Name = c.String(),
                        StreetName = c.String(),
                        StreetNumber = c.String(),
                        ZipCode = c.String(),
                        City = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.CustomerID);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        FilmID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Poster = c.String(),
                        Director = c.String(),
                        Language = c.String(),
                        length = c.Int(nullable: false),
                        Type = c.String(),
                        Trailer = c.String(),
                        Age = c.String(),
                        SubTitles = c.String(),
                    })
                .PrimaryKey(t => t.FilmID);
            
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        PriceID = c.Int(nullable: false, identity: true),
                        TicketPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TicketPriceLong = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Name = c.String(),
                        DisplayName = c.String(),
                        Description = c.String(),
                        DiscountChild = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscountStudent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscountSenior = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Addition3D = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PriceID);
            
            CreateTable(
                "dbo.Reservations",
                c => new
                    {
                        TicketID = c.Int(nullable: false, identity: true),
                        ReservationID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                        RowNumber = c.Int(nullable: false),
                        SeatNumber = c.Int(nullable: false),
                        ScheduleID = c.Int(nullable: false),
                        Paid = c.Boolean(nullable: false),
                        Printed = c.Boolean(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.TicketID, t.ReservationID })
                .ForeignKey("dbo.Schedules", t => t.ScheduleID, cascadeDelete: true)
                .Index(t => t.ScheduleID);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        ScheduleID = c.Int(nullable: false, identity: true),
                        RoomID = c.Int(nullable: false),
                        FilmID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.ScheduleID)
                .ForeignKey("dbo.Films", t => t.FilmID, cascadeDelete: true)
                .ForeignKey("dbo.Rooms", t => t.RoomID, cascadeDelete: true)
                .Index(t => t.RoomID)
                .Index(t => t.FilmID);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        RoomNumber = c.Int(nullable: false, identity: true),
                        RoomLayout = c.String(),
                        Type = c.String(),
                        WheelChair = c.Boolean(nullable: false),
                        TotalSeats = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoomNumber);
            
        }
        
        public override void Down()
        {
            
        }
    }
}
