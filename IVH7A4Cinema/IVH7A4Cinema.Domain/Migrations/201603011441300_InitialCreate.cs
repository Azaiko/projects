namespace IVH7A4Cinema.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Name = c.String(),
                        StreetName = c.String(),
                        StreetNumber = c.String(),
                        ZipCode = c.String(),
                        City = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.CustomerID);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        FilmID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Poster = c.String(),
                        Director = c.String(),
                        Language = c.String(),
                        length = c.Int(nullable: false),
                        Type = c.String(),
                        Trailer = c.String(),
                    })
                .PrimaryKey(t => t.FilmID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Films");
            DropTable("dbo.Customers");
        }
    }
}
