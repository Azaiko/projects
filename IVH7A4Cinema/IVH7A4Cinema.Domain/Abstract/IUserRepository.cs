﻿using System;
using System.Collections.Generic;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.Domain.Abstract
{
    public interface IUserRepository
    {
        IEnumerable<User> Users { get; }
    }
}
