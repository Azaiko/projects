﻿using IVH7A4Cinema.Domain.Entities;
using System.Collections.Generic;

namespace IVH7A4Cinema.Domain.Abstract
{
    public interface IPriceRepository
    {
        IEnumerable<Price> Prices { get; }
    }
}
