﻿using System.Collections.Generic;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.Domain.Abstract
{
    public interface IReservationRepository
    {
        IEnumerable<Reservation> Reservations { get; }

        void SaveReservation(Reservation Reservation);
    }
}
