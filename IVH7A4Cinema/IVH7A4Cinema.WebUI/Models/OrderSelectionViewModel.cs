﻿using IVH7A4Cinema.Domain.Entities;
using System.Collections.Generic;

namespace IVH7A4Cinema.WebUI.Models
{
    public class OrderSelectionViewModel
    {
        public IEnumerable<Price> Prices { get; set; }
        public Schedule Schedule {get; set; }
        public Film Film { get; set; }
    }

}