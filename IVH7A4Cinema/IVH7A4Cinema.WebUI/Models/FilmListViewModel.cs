﻿using IVH7A4Cinema.Domain.Entities;
using System.Collections.Generic;

namespace IVH7A4Cinema.WebUI.Models
{
    public class FilmListViewModel
    {
        public IEnumerable<Film> Films { get; set; }  
    }
}