﻿using IVH7A4Cinema.Domain.Entities;
using System.Collections.Generic;

namespace IVH7A4Cinema.WebUI.Models
{
    public class PlaceOrderViewModel
    {
        public List<Ticket> Tickets { get; set; }
        public Dictionary<string, bool> SelectedSeats { get; set; }
        public Schedule Schedule { get; set; }
    }
}