﻿using IVH7A4Cinema.Domain.Entities;
using System.Collections.Generic;

namespace IVH7A4Cinema.WebUI.Models
{
    public class RoomLayoutModel
    {
        public int NeededSeats { get; set; }
        public string[] Rows { get; set; }
        public IEnumerable<Reservation> Reservations { get; set; }
        public Schedule Schedule { get; set; }
        public List<Ticket> Tickets { get; set; }
    }
}