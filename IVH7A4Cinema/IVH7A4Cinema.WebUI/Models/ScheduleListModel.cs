﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.WebUI.Models
{
    public class ScheduleListModel
    {
        public Schedule Schedule { get; set; }
    }
}