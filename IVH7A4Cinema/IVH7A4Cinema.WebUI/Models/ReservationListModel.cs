﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.WebUI.Models
{
    public class ReservationListModel
    {
        public int ReservationId { get; set; }
        public Schedule Schedule { get; set; }
        public IEnumerable<Reservation> Reservations { get; set; }
    }
}