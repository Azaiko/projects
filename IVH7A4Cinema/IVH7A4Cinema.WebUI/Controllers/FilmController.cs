﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class FilmController : Controller
    {
        private IFilmRepository repository;
        private IReservationRepository reservationRepository;
        private IRoomRepository roomRepository;

        public FilmController(IRoomRepository roomRepository, IFilmRepository filmRepository, IReservationRepository reservationRepository)
        {
            this.repository = filmRepository;
            this.reservationRepository = reservationRepository;
            this.roomRepository = roomRepository;
        }

        public ViewResult SchedulePlanning()
        {
            var schedules = repository.Schedules;
            IEnumerable<Film> filmsToday = repository.Films
                .Join(schedules, f => f.FilmID, s => s.FilmID, (f, s) => new { Film = f, Schedule = s })
                .Where(s => s.Schedule.Date == DateTime.Now.Date)                
                .GroupBy(film => film.Film.Name,(key, film) => film.FirstOrDefault())
                .Select(fg => new Film { FilmID = fg.Film.FilmID, Name = fg.Film.Name,
                Description = fg.Film.Description, Director = fg.Film.Director,
                Language = fg.Film.Language, length = fg.Film.length, Type = fg.Film.Type,
                Trailer = fg.Film.Trailer, Age = fg.Film.Age, SubTitles = fg.Film.SubTitles})
                .Distinct()
                .ToList();

            FilmListViewModel model = new FilmListViewModel
            {
                Films = filmsToday
            };

            return View(model);
        }
        [ExcludeFromCodeCoverage]
        public PartialViewResult FilmSchedulePlanning(Film Film)
        {
            try
            {
                Schedule schedule = repository.Schedules
                    .Where(s => s.Date == DateTime.Now.Date && s.FilmID == Film.FilmID && s.Time > DateTime.Now.TimeOfDay)
                    .OrderBy(s => s.Time).First();


            FilmScheduleModel model = new FilmScheduleModel
            {
                Film = Film,
                Schedule = schedule,
                Room = roomRepository.Rooms.Where(r => r.RoomNumber == schedule.RoomID).FirstOrDefault()
            };

            return PartialView(model);
            }
            catch (Exception ex)
            { }
            return null;
        }
        [ExcludeFromCodeCoverage]
        public PartialViewResult Schedule(int FilmID, Schedule Schedule, Room Room)
        {
            IEnumerable<Schedule> schedules = repository.Schedules
                .Where(s => s.Date == DateTime.Now.Date && s.FilmID == FilmID && s.Time > DateTime.Now.TimeOfDay)
                .OrderBy(s => s.Time);
            return PartialView(schedules);
        }
    }
}