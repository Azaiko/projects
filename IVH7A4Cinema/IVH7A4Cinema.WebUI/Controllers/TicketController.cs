﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;
using Rotativa;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class TicketController : Controller
    {
        private IReservationRepository reservationRepository;

        public TicketController(IReservationRepository reservationRepository)
        {
            this.reservationRepository = reservationRepository;
        }

        // GET: Ticket
        public ViewResult TicketOverview(int ReservationId)
        {

            IEnumerable<Reservation> Reservations = reservationRepository.Reservations.Where(r => r.ReservationID == ReservationId);

            Reservation reservation = reservationRepository
    .Reservations
    .Where(r => r.ReservationID == ReservationId)
    .FirstOrDefault();


            Schedule schedule = reservation.Schedule;

            ReservationListModel model = new ReservationListModel
            {
                ReservationId = ReservationId,
                Schedule = schedule,
                Reservations = Reservations
            };

            return View(model);
        }

        public ActionResult TicketPDF(int ReservationId)
        {
            IEnumerable<Reservation> Reservations = reservationRepository.Reservations.Where(r => r.ReservationID == ReservationId);

            return View("PdfTicket", Reservations);
        }

        public ActionResult Pdf(int ReservationId = 2)
        {
            return new ActionAsPdf("TicketPDF", new { ReservationId = ReservationId })
            {
                FileName = "Ticket.pdf"
            };
        }
    }
}