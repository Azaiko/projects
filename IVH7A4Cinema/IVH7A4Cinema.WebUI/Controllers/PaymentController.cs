﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class PaymentController : Controller
    {
        private IReservationRepository reservationRepository;

        public PaymentController(IReservationRepository reservationRepository)
        {
            this.reservationRepository = reservationRepository;
        }

        public ActionResult PaymentCheck(int ReservationId)
        {
            Random rnd = new Random();
            int pincheck = rnd.Next(1, 11);
            int rID = ReservationId;

            IEnumerable<Reservation> Reservations = reservationRepository.Reservations.Where(r => r.ReservationID == ReservationId).ToList();

            if (pincheck <= 9)
            {
                ViewBag.PaymentMsg = "De betaling is succesvol!";

                foreach (var r in Reservations)
                {
                    r.Paid = true;
                    reservationRepository.SaveReservation(r);
                    return RedirectToAction("TicketOverview", "Ticket", new { ReservationId = r.ReservationID });
                }
                return View("PaymentSuccess");

            }
            else
            {
                ViewBag.PaymentMsg = "De betaling is mislukt!";
            }
            return View("PaymentFail");


        }
        /* public ActionResult ToTickets(int rID)
        {
            IEnumerable<Reservation> Reservations = reservationRepository.Reservations.Where(r => r.ReservationID == rID).ToList();

                foreach (var r in Reservations)
                {
                return RedirectToAction("TicketOverview", "Ticket", new { ReservationId = r.ReservationID });
            }
            return null;
        }
        */

    }
}