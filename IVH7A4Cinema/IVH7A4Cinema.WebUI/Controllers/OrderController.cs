﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class OrderController : Controller
    {
        
        private IPriceRepository PriceRepository;
        private IFilmRepository FilmRepository;
        private IReservationRepository ReservationRepository;
       
        //Repository voor de prijs aanmaken
        public OrderController(IPriceRepository priceRepository, IFilmRepository filmRepository, IReservationRepository reservationRepository)
        {
            this.PriceRepository = priceRepository;
            this.FilmRepository = filmRepository;
            this.ReservationRepository = reservationRepository;
        }
        
        [HttpGet]
        // GET: Ticket
        
        public ActionResult OrderSelection(Schedule schedule)
        {
            if (schedule == null)
            {
                return this.HttpNotFound();
            }
            OrderSelectionViewModel model = new OrderSelectionViewModel
            {
                Schedule = schedule,
                Prices = PriceRepository.Prices,
                Film = FilmRepository.Films.Where(f => f.FilmID == schedule.FilmID).FirstOrDefault()
            };
            return View("OrderSelection", model);
        }
        [ExcludeFromCodeCoverage]
        public int getTotalSeats(FormCollection formCollection)
        {
            //Get total tickets from the formCollection and assign it to totalseats.
            int totalSeats = 0;
            foreach (string sKey in formCollection.AllKeys)
            {
                string tickets = formCollection[sKey];
                try
                {
                    if (sKey.Contains("textField"))
                    {
                        totalSeats += int.Parse(tickets);
                        
                    }
                }
                catch (System.Exception ex)
                {
                        
                }
            }
            return totalSeats;
        }

        [HttpPost]
        public ActionResult OrderSelection(FormCollection formCollection)
        {

            List<Ticket> TicketsList = new List<Ticket>();

            foreach (string sKey in formCollection.AllKeys)
            {
                string tickets = formCollection[sKey];

                if (sKey.Contains("textField"))
                {
                    int Amount = int.Parse(tickets);

                    if (Amount > 0)
                    {
                        string PriceName = sKey.Replace("textField", "");
                        Price Price = PriceRepository.Prices.Where(p => p.Name == PriceName).FirstOrDefault();
                        TicketsList.Add(new Ticket { Amount = Amount, Price = Price, PriceID = Price.PriceID });

                    }
                }

            }
            TempData["TicketList"] = TicketsList;
            return RedirectToAction("RenderRoomLayout", "Room", new { scheduleID = formCollection["scheduleID"], neededSeats = getTotalSeats(formCollection) });
        }

        public ViewResult PlaceOrder()
        {
            List<Ticket> Tickets = TempData["TicketList"] as List<Ticket>;
            Dictionary<string, bool> SelectedSeat = TempData["SelectedSeat"] as Dictionary<string, bool>;
            Schedule Schedule = TempData["Schedule"] as Schedule;

            PlaceOrderViewModel Model = new PlaceOrderViewModel
            {
                Tickets = Tickets,
                SelectedSeats = SelectedSeat,
                Schedule = Schedule
            };

            TempData["PlaceOrderViewModel"] = Model;

            return View("PlaceOrder", Model);
        }

        public ActionResult Order(PlaceOrderViewModel Model)
        {
            Model = TempData["PlaceOrderViewModel"] as PlaceOrderViewModel;
            Random random = new Random();
            int ReservationId = random.Next(10000000, 99999999);
            var check = ReservationRepository.Reservations.Where(r => r.ReservationID == ReservationId).FirstOrDefault();
            if (check != null)
            {
                ReservationId = random.Next(10000000, 99999999);
            }

            foreach (var seat in Model.SelectedSeats)
            {
                if (seat.Value != false)
                {
                    string[] RowSeat = seat.Key.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    Reservation Reservation = new Reservation();

                    Reservation.ReservationID = ReservationId;
                    Reservation.CustomerID = 0;
                    Reservation.RowNumber = Int32.Parse(RowSeat[0]);
                    Reservation.SeatNumber = Int32.Parse(RowSeat[1]);
                    Reservation.ScheduleID = Model.Schedule.ScheduleID;
                    Reservation.Paid = false;
                    Reservation.Printed = false;

                    ReservationRepository.SaveReservation(Reservation);
                }
            }
            return RedirectToAction("PaymentCheck", "Payment", new { ReservationId = ReservationId });
        }
    }
}