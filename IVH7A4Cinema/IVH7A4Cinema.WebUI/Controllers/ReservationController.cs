﻿using System.Linq;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.Domain.Abstract;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class ReservationController : Controller
    {
        private IReservationRepository repository;

        // Repository aanmaken
        public ReservationController(IReservationRepository reservationRepository)
        {
            this.repository = reservationRepository;

        }
        
        // GET: Reservation
        [HttpGet]
        public ViewResult ReservationHome()
        {
            return View("ReservationHome");
        }

        [HttpPost]
        public ActionResult ReservationCheck(Reservation reservation)
        {
            bool result = ReservationIDCheck(reservation.ReservationID);

            if (result == false)
            {
                ViewBag.Error = "De ingevoerde ID is ongeldig, raadpleeg een medewerker als u het ID correct heeft ingevoerd.";
                return View("ReservationHome");
            }
            else
            {
                //Moet de reservation niet opnieuw kunnen ophalen
                //Moet checken of payment nog nodig is, zo ja moet hij doorverwijzen naar payment. Zo nee moet hij kaartje printen
                //Moet de stoelen bevestigen
                reservation = repository.Reservations.Where(r => r.ReservationID == reservation.ReservationID).FirstOrDefault();

                if (reservation.Paid == true && reservation.Printed == false)
                {
                    // Reservering is betaald nog niet geprint
                    return RedirectToAction("TicketOverview", "Ticket", new { ReservationId  = reservation.ReservationID});
                    //return DependencyResolver.Current.GetService<TicketController>().TicketOverview(reservation.ReservationID);
                }else if (reservation.Printed == false && reservation.Printed == false)
                {
                    // Reservering is niet betaald en niet geprint
                    return RedirectToAction("PaymentCheck", "Payment", new { ReservationId = reservation.ReservationID });                  
                }
                else
                {
                 
                    // Reservering is al geprint
                    return RedirectToAction("Index", "Home");
                    //return DependencyResolver.Current.GetService<PaymentController>().PaymentCheck(reservation);
                }               
            }
        }

        //Haalt de reservation ID's op uit de database en checked of deze gelijk is aan ingevoerde waarde 
        public bool ReservationIDCheck(int reservationNr)
        {
            bool result = false;
            Reservation reservation = repository.Reservations.Where(r => r.ReservationID == reservationNr).FirstOrDefault();

            if (reservation != null) {
                if (reservation.ReservationID == reservationNr)
                {
                    result = true;
                }
            }
            else {
                result = false;
            }
            return result;
        }
    }
}