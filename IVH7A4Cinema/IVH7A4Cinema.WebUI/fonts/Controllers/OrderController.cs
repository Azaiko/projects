﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;
using System.Linq;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class OrderController : Controller
    {
        
        private IPriceRepository PriceRepository;
        private IFilmRepository FilmRepository;
       
        //Repository voor de prijs aanmaken
        public OrderController(IPriceRepository priceRepository, IFilmRepository filmRepository)
        {
            this.PriceRepository = priceRepository;
            this.FilmRepository = filmRepository;
        }
        
        [HttpGet]
        // GET: Ticket
        
        public ActionResult OrderSelection(Schedule schedule)
        {
            if(schedule == null)
            {
                return this.HttpNotFound();
            }
            OrderSelectionViewModel model = new OrderSelectionViewModel
            {
                Schedule = schedule,
                Prices = PriceRepository.Prices,
                Film = FilmRepository.Films.Where(f => f.FilmID == schedule.FilmID).FirstOrDefault()
            };
            return View("OrderSelection", model);
        }

        [HttpPost]
        public ActionResult OrderSelection(FormCollection formCollection)
        {

            int totalSeats = 0;
            foreach (string sKey in formCollection.AllKeys)
            {
                string tickets = formCollection[sKey];
                try
                {   
                    if (sKey.Contains("textField"))
                    {
                        totalSeats += int.Parse(tickets);
                    }
                }
                catch (System.Exception ex) { }
            }


            //return RedirectToAction("RenderRoomLayout", "Room", new { scheduleID = formCollection["scheduleID"], neededSeats = formCollection["textFieldNormal"] });
            return RedirectToAction("RenderRoomLayout", "Room", new { scheduleID = formCollection["scheduleID"],  neededSeats = totalSeats});
        }

        // wanneer er een film in .schedule zit wordt de lengte van de film eruit gehaald en teruggegeven
        public int MovieDuration(Schedule schedule)
        {
            Film film = FilmRepository.Films.Where(f => f.FilmID == schedule.FilmID).FirstOrDefault();

            if(film != null)
            {
                    return film.length;
            }
            return 0;
        }

        // wanneer er een film in schedule zit wordt de naam van de film eruit gehaald en teruggegeven
        public string MovieName(Schedule schedule)
        {
            Film film = FilmRepository.Films.Where(f => f.FilmID == schedule.FilmID).FirstOrDefault();

            if (film != null)
            {
                    return film.Name;
            }
            return null;
        }

        [HttpPost]
        public ActionResult Numbers(string textFieldNormal)
        {
            //check for reportName parameter value now
            //to do : Return something
            return View();
        }
    }
}