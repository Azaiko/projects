﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class RoomController : Controller
    {
        private IRoomRepository repository;
        private IReservationRepository reservationRepository;
        private IFilmRepository filmRepository;

        public RoomController(IRoomRepository roomRepository, IReservationRepository reservationRepository, IFilmRepository filmRepository)
        {
            this.repository = roomRepository;
            this.reservationRepository = reservationRepository;
            this.filmRepository = filmRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RenderRoomLayout(int scheduleID, int neededSeats = 1)
        {
            Schedule schedule = filmRepository.Schedules.Where(s => s.PlanningID == scheduleID).FirstOrDefault();
            Room room = repository.Rooms.Where(r => r.RoomNumber == schedule.RoomID).FirstOrDefault();

            string rowVar = room.RoomLayout;
            rowVar = rowVar.Replace(" ", "");
            string[] row = rowVar.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            RoomLayoutModel model = new RoomLayoutModel
            {
                NeededSeats = neededSeats,
                Rows = row,
                Reservations = reservationRepository.Reservations.Where(r => r.ScheduleID == schedule.PlanningID)
            };
            
            return View(model);
        }

        /*[HttpPost]
        public ActionResult Cart(FormCollection formCollection)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            /*CartOrderViewModel model = new CartOrderViewModel
            {
                SelectedSeat = js.Deserialize<Seat>(formCollection["seats"])
            };


            return View();
        }*/
    }
}