﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.WebUI.Models;
using IVH7A4Cinema.Domain.Concrete;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class ReservationController : Controller
    {
        private IReservationRepository repository;
        private bool idValidation;

        // Repository aanmaken
        public ReservationController(IReservationRepository reservationRepository)
        {
            this.repository = reservationRepository;
        }
        
        // GET: Reservation
        [HttpGet]
        public ViewResult ReservationHome(Schedule schedule)
        {
            return View();
        }

        [HttpPost]
        public ActionResult ReservationCheck(Reservation reservationNr)
        {
            ReservationIDCheck(reservationNr);

            if (idValidation == false)
            {
                ViewBag.Error = ("De ingevoerde ID is ongeldig, raadpleeg een medewerker als u het ID correct heeft ingevoerd.");
                return View("~/Views/Reservation/ReservationHome.cshtml");
            }
            else if (idValidation == true)
            {
                //Moet de reservation niet opnieuw kunnen ophalen
                //Moet checken of payment nog nodig is, zo ja moet hij doorverwijzen naar payment. Zo nee moet hij kaartje printen
                //Moet de stoelen bevestigen
                return View("~/Views/Home/Index.cshtml");
            }
            else {
                return View("~/Views/Reservation/ReservationHome.cshtml");
            }
        }

        //Haalt de reservation ID's op uit de database en checked of deze gelijk is aan ingevoerde waarde 
        public bool ReservationIDCheck(Reservation reservationNr)
        {
            Reservation reservation = repository.Reservations.Where(r => r.ReservationID == reservationNr.ReservationID).FirstOrDefault();

            if (reservation != null) {
                if (reservation.ReservationID == reservationNr.ReservationID)
                {
                    idValidation = true;
                }
                else {
                    idValidation = false;
                }
            }
            else {
                idValidation = false;
            }
            return idValidation;
        }
    }
}