﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class PaymentController : Controller
    {

        public ViewResult PaymentCheck()
        {
            Random rnd = new Random();
            int pincheck = rnd.Next(1, 11);

            if (pincheck <= 9)
            {
                ViewBag.PaymentMsg("De betaling is succesvol!");
            }
            else
            {
                ViewBag.PaymentMsg("De betaling is mislukt!");
            }
            return View();
        }
    }
}