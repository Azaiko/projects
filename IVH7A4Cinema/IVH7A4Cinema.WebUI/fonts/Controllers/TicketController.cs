﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebUI.Models;

namespace IVH7A4Cinema.WebUI.Controllers
{
    public class TicketController : Controller
    {
        private IScheduleRepository scheduleRepository;
        private IFilmRepository filmRepository;
        private IReservationRepository reservationRepository;
        private IRoomRepository roomRepository;

        public TicketController(IScheduleRepository scheduleRepository, IFilmRepository filmRepository, IReservationRepository reservationRepository, IRoomRepository roomRepository)
        {
            this.scheduleRepository = scheduleRepository;
            this.filmRepository = filmRepository;
            this.reservationRepository = reservationRepository;
            this.roomRepository = roomRepository;
        }

        // GET: Ticket
        public ActionResult TicketOverview()
        {
            Reservation reservation = reservationRepository
                .Reservations
                .Where(r => r.ReservationID == 2)
                .FirstOrDefault();
            Schedule schedule = scheduleRepository
                .Schedules
                .Where(s => s.PlanningID == reservation.ScheduleID)
                .FirstOrDefault();
            Film film = filmRepository
                .Films
                .Where(f => f.FilmID == schedule.FilmID)
                .FirstOrDefault();
            Room room = roomRepository
                .Rooms
                .Where(ro => ro.RoomNumber == schedule.RoomID)
                .FirstOrDefault();

            IEnumerable<Reservation> reservationsAll = reservationRepository.Reservations;
            //.Where(ReservationListModel.);
            //.Any(a => a.ReservationID == 1);

            //.Where(a => a.ReservationID == 1);

            //var reservations = (reservationsAll = reservationsAll.Where(a=>a.ReservationID ==1)).ToList();

            ReservationListModel model = new ReservationListModel
            {
                Schedule = schedule,
                Films = film,
                Reservation = reservation,
                Rooms = room,
                ReservationsAll = reservationsAll
            };

            return View(model);
        }
    }
}