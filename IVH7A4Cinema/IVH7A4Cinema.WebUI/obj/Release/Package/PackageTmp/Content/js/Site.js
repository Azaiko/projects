﻿function updateScherm() {
    var tijd = new Date();

    // Klok updaten:
    uren = tijd.getHours();
    min = tijd.getMinutes();
    if (uren < 10) {
        uren = '0' + uren;
    }
    if (min < 10) {
        min = '0' + min;
    }

    // Knipperende dubbele punt:
    if (tijd.getSeconds() % 2 == 0) {
        document.getElementById('real_time').innerHTML = uren + '<span>:</span>' + min;
    } else {
        document.getElementById('real_time').innerHTML = uren + '<span style="visibility: hidden">:</span>' + min;
    }

    setTimeout(updateScherm, 500);
}

$(document).ready(function () {
    updateScherm();
});
