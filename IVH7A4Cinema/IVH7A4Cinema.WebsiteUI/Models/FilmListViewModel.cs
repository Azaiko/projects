﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IVH7A4Cinema.Domain.Entities;


namespace IVH7A4Cinema.WebsiteUI.Models
{
    public class FilmListViewModel
    {
        public IEnumerable<Film> Films { get; set; }

        public Film film { get; set; }
    }
}