﻿using IVH7A4Cinema.Domain.Entities;
using System.Collections.Generic;

namespace IVH7A4Cinema.WebUI.Models
{
    public class CartOrderViewModel
    {
        public Dictionary<string, bool> SelectedSeat { get; set; }

        public int NeededSeats { get; set; }
    }
}