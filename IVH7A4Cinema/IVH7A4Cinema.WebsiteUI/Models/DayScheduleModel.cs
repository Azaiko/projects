﻿using IVH7A4Cinema.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IVH7A4Cinema.WebsiteUI.Models
{
    public class DayScheduleModel
    {
        public IEnumerable<Film> FilmsOfDay { get; set; }
        public DateTime Date { get; set; }
    }
}