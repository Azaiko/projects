﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.WebsiteUI.Models
{
    public class OrderSelectionViewModel
    {
        public IEnumerable<Price> Prices { get; set; }
        public Schedule Schedule { get; set; }
        public Film Film { get; set; }
    }
}