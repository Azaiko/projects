﻿using IVH7A4Cinema.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IVH7A4Cinema.WebsiteUI.Models
{
    public class SearchViewModel
    {
        public IEnumerable<Film> Films { get; set; }

        public string CurrentSearchString { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CurrentDateSearch { get; set; }

        public TimeSpan CurrentTimeSearch { get; set; }
    }
}