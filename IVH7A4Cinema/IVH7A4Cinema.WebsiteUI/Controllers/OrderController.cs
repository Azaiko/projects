﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebsiteUI.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebsiteUI.Controllers
{
    public class OrderController : Controller
    {
        
        private IPriceRepository PriceRepository;
        private IFilmRepository FilmRepository;
        private IReservationRepository ReservationRepository;
       
        //Repository voor de prijs aanmaken
        public OrderController(IPriceRepository priceRepository, IFilmRepository filmRepository, IReservationRepository reservationRepository)
        {
            this.PriceRepository = priceRepository;
            this.FilmRepository = filmRepository;
            this.ReservationRepository = reservationRepository;
        }
        
        [ExcludeFromCodeCoverage]
        public int getTotalSeats(FormCollection formCollection)
        {
            //Get total tickets from the formCollection and assign it to totalseats.
            int totalSeats = 0;
            foreach (string sKey in formCollection.AllKeys)
            {
                string tickets = formCollection[sKey];
                try
                {
                    if (sKey.Contains("textField"))
                    {
                        totalSeats += int.Parse(tickets);
                        
                    }
                }
                catch (System.Exception ex)
                {
                        
                }
            }
            return totalSeats;
        }

        public ActionResult Order(PlaceOrderViewModel Model)
        {
            Model = TempData["PlaceOrderViewModel"] as PlaceOrderViewModel;
            Random random = new Random();
            int ReservationId = random.Next(10000000, 99999999);
            var check = ReservationRepository.Reservations.Where(r => r.ReservationID == ReservationId).FirstOrDefault();
            if (check != null)
            {
                ReservationId = random.Next(10000000, 99999999);
            }

            foreach (var seat in Model.SelectedSeats)
            {
                if (seat.Value != false)
                {
                    string[] RowSeat = seat.Key.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    Reservation Reservation = new Reservation();

                    Reservation.ReservationID = ReservationId;
                    Reservation.CustomerID = 0;
                    Reservation.RowNumber = Int32.Parse(RowSeat[0]);
                    Reservation.SeatNumber = Int32.Parse(RowSeat[1]);
                    Reservation.ScheduleID = Model.Schedule.ScheduleID;
                    Reservation.Paid = false;
                    Reservation.Printed = false;

                    ReservationRepository.SaveReservation(Reservation);
                }
            }
            return RedirectToAction("Index", "Payment", new { ReservationId = ReservationId });
        }
    }
}