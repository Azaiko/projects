﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebsiteUI.Models;
using System.Globalization;
using System.Threading;

namespace IVH7A4Cinema.WebsiteUI.Controllers
{
    public class HomeController : Controller
    {
        private IFilmRepository FilmRepository;
        private IReservationRepository reservationRepository;

        public HomeController(IFilmRepository filmRepository, IReservationRepository reservationRepository)
        {
            this.FilmRepository = filmRepository;
            this.reservationRepository = reservationRepository;
        }

        public ViewResult Index()
        {
            CultureInfo ci = new CultureInfo("nl-NL");
            Thread.CurrentThread.CurrentCulture = ci;

            return View("Index");
        }

        public PartialViewResult DaySchedule(DateTime Date)
        {
            IEnumerable<Film> FilmOfDay = FilmRepository.Films
                .Join(FilmRepository.Schedules, f => f.FilmID, s => s.FilmID, (f, s) => new { Film = f, Schedule = s })
                .Where(s => s.Schedule.Date == Date)
                .GroupBy(film => film.Film.Name, (key, film) => film.FirstOrDefault())
                .Select(fg => new Film
                {
                    FilmID = fg.Film.FilmID,
                    Name = fg.Film.Name,
                    Description = fg.Film.Description,
                    Director = fg.Film.Director,
                    Language = fg.Film.Language,
                    length = fg.Film.length,
                    Type = fg.Film.Type,
                    Trailer = fg.Film.Trailer,
                    Age = fg.Film.Age,
                    SubTitles = fg.Film.SubTitles
                })
                .Distinct()
                .ToList();
            DayScheduleModel Model = new DayScheduleModel
            {
                FilmsOfDay = FilmOfDay,
                Date = Date
            };
            return PartialView(Model);
        }

        public PartialViewResult Schedule(int FilmID, DateTime Date)
        {
            IEnumerable<Schedule> schedules = FilmRepository.Schedules
                .Where(s => s.Date == Date && s.FilmID == FilmID && s.Time > DateTime.Now.TimeOfDay)
                .OrderBy(s => s.Time);
            return PartialView("Schedule", schedules);
        }
    }
}