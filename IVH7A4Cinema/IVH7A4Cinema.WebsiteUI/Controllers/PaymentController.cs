﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa;

namespace IVH7A4Cinema.WebsiteUI.Controllers
{
    public class PaymentController : Controller
    {

        private IReservationRepository reservationRepository;

        public PaymentController(IReservationRepository reservationRepository)
        {
            this.reservationRepository = reservationRepository;
        }

        public ViewResult Index(int ReservationId)
        {
            ViewBag.ReservationId = ReservationId;
            return View("Index");
        }

        [HttpPost]
        public ActionResult Index(string paymentMethod, int ReservationId)
        {

            IEnumerable<Reservation> reservations = reservationRepository.Reservations.Where(r => r.ReservationID == ReservationId).ToList();

            foreach(Reservation reservation in reservations) { 
                switch (paymentMethod)
                {
                    case "IDEAL":
                    case "CREDIT_CARD":
                        reservation.Paid = true;
                        reservationRepository.SaveReservation(reservation);
                        break;
                    case "LATER":
                        break;
                }
            }
            return RedirectToAction("Confirmation", "Payment", new { ReservationId = ReservationId });
        }

        public ViewResult Confirmation(int ReservationId)
        {
            return View("Confirmation", ReservationId);
        }

        public ActionResult ConfirmationPDF(int ReservationId)
        {
            Reservation Reservation = reservationRepository.Reservations.Where(r => r.ReservationID == ReservationId).FirstOrDefault();

            return View("PdfConfirmation", Reservation);
        }

        public ActionResult Pdf(int ReservationId)
        {
            return new ActionAsPdf("ConfirmationPDF", new { ReservationId = ReservationId })
            {
                FileName = "Bevestiging_" + ReservationId+ ".pdf"
            };
        }

        public ViewResult Pay()
        {
            return View();
        }
    }
}