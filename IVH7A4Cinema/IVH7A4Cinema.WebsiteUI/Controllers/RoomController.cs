﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebsiteUI.Models;
using IVH7A4Cinema.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IVH7A4Cinema.WebsiteUI.Controllers
{
    public class RoomController : Controller
    {
        private IRoomRepository repository;
        private IReservationRepository reservationRepository;
        private IFilmRepository filmRepository;

        public RoomController(IRoomRepository roomRepository, IReservationRepository reservationRepository, IFilmRepository filmRepository)
        {
            this.repository = roomRepository;
            this.reservationRepository = reservationRepository;
            this.filmRepository = filmRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RenderRoomLayout(int scheduleID, int neededSeats = 1)
        {
            List<Ticket> Tickets = TempData["TicketList"] as List<Ticket>;
            Schedule Schedule = filmRepository.Schedules.Where(s => s.ScheduleID == scheduleID).FirstOrDefault();
            Room room = repository.Rooms.Where(r => r.RoomNumber == Schedule.RoomID).FirstOrDefault();

            string rowVar = room.RoomLayout;
            rowVar = rowVar.Replace(" ", "");
            string[] row = rowVar.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            RoomLayoutModel model = new RoomLayoutModel
            {
                NeededSeats = neededSeats,
                Tickets = Tickets,
                Rows = row,
                Reservations = reservationRepository.Reservations.Where(r => r.ScheduleID == Schedule.ScheduleID),
                Schedule = Schedule
            };
            TempData["Schedule"] = Schedule;
            return View(model);
        }

        [HttpPost]
        public ActionResult Cart(FormCollection formCollection)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            var seats = js.Deserialize(formCollection["seats"], typeof(object));
            Dictionary<string, bool> SelectedSeat = js.Deserialize<Dictionary<string, bool>>(formCollection["seats"]);

            CartOrderViewModel model = new CartOrderViewModel
            {
                SelectedSeat = SelectedSeat,
                NeededSeats = Int32.Parse(formCollection["neededSeats"])
            };

            TempData["SelectedSeat"] = SelectedSeat;

            List<Ticket> Tickets = TempData["TicketList"] as List<Ticket>;
            Schedule Schedule = TempData["Schedule"] as Schedule;

            PlaceOrderViewModel Model = new PlaceOrderViewModel
            {
                Tickets = Tickets,
                SelectedSeats = SelectedSeat,
                Schedule = Schedule
            };

            TempData["Schedule"] = Schedule;
            TempData["PlaceOrderViewModel"] = Model;

            return View(model);
        }
    }
}