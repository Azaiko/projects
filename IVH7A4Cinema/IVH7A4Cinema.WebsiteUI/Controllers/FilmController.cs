﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.WebsiteUI.Models;


namespace IVH7A4Cinema.WebsiteUI.Controllers
{

    public class FilmController : Controller
    {
        private IFilmRepository repository;
        public FilmController(IFilmRepository filmRepository)
        {
            this.repository = filmRepository;
        }
        // GET: Film
        public ViewResult FilmSelection()
        {
            IEnumerable<Film> films = repository.Films
                                .Select(fg => new Film
                                {
                                    FilmID = fg.FilmID,
                                    Name = fg.Name,
                                    Description = fg.Description,
                                    Director = fg.Director,
                                    Language = fg.Language,
                                    length = fg.length,
                                    Type = fg.Type,
                                    Trailer = fg.Trailer,
                                    Age = fg.Age,
                                    SubTitles = fg.SubTitles
                                });

                FilmListViewModel model = new FilmListViewModel
                {
                    Films = films
                };
            return View("FilmSelection", model);
        }

        public ViewResult FilmDetail(int FilmID)
        {
            Film selectedFilm = repository.Films.Where(f => f.FilmID == FilmID).First();
            return View("FilmDetail", selectedFilm);
        }
    }
}