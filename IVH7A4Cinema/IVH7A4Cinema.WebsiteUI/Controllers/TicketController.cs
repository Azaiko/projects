﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebsiteUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebsiteUI.Controllers
{
    public class TicketController : Controller
    {
        private IPriceRepository PriceRepository;
        private IFilmRepository FilmRepository;
        private IReservationRepository ReservationRepository;

        public TicketController(IPriceRepository priceRepository, IFilmRepository filmRepository, IReservationRepository reservationRepository)
        {
            this.PriceRepository = priceRepository;
            this.FilmRepository = filmRepository;
            this.ReservationRepository = reservationRepository;
        }

        // GET: Ticket
        public ActionResult TicketSelection(Schedule schedule)
        {
            if (schedule == null)
            {
                return this.HttpNotFound();
            }
            OrderSelectionViewModel model = new OrderSelectionViewModel
            {
                Schedule = schedule,
                Prices = PriceRepository.Prices,
                Film = FilmRepository.Films.Where(f => f.FilmID == schedule.FilmID).FirstOrDefault()
            };
            return View("TicketSelection", model);
        }

        public int getTotalSeats(FormCollection formCollection)
        {
            //Get total tickets from the formCollection and assign it to totalseats.
            int totalSeats = 0;
            foreach (string sKey in formCollection.AllKeys)
            {
                string tickets = formCollection[sKey];
                try
                {
                    if (sKey.Contains("textField"))
                    {
                        totalSeats += int.Parse(tickets);
                    }
                }
                catch (System.Exception ex)
                {

                }
            }
            return totalSeats;
        }

        [HttpPost]
        public ActionResult TicketSelection(FormCollection formCollection)
        {
            List<Ticket> TicketsList = new List<Ticket>();

            foreach (string sKey in formCollection.AllKeys)
            {
                string tickets = formCollection[sKey];

                if (sKey.Contains("textField"))
                {
                    int Amount = int.Parse(tickets);

                    if (Amount > 0)
                    {
                        string PriceName = sKey.Replace("textField", "");
                        Price Price = PriceRepository.Prices.Where(p => p.Name == PriceName).FirstOrDefault();
                        TicketsList.Add(new Ticket { Amount = Amount, Price = Price, PriceID = Price.PriceID });
                    }
                }

            }
            TempData["TicketList"] = TicketsList;
            return RedirectToAction("RenderRoomLayout", "Room", new { scheduleID = formCollection["scheduleID"], neededSeats = getTotalSeats(formCollection) });
        }

        public ViewResult PlaceOrder()
        {
            List<Ticket> Tickets = TempData["TicketList"] as List<Ticket>;
            Dictionary<string, bool> SelectedSeat = TempData["SelectedSeat"] as Dictionary<string, bool>;
            Schedule Schedule = TempData["Schedule"] as Schedule;

            PlaceOrderViewModel Model = new PlaceOrderViewModel
            {
                Tickets = Tickets,
                SelectedSeats = SelectedSeat,
                Schedule = Schedule
            };

            TempData["PlaceOrderViewModel"] = Model;

            return View("PlaceOrder", Model);
        }
    }
}