﻿using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Entities;
using IVH7A4Cinema.WebsiteUI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace IVH7A4Cinema.WebsiteUI.Controllers
{
    public class SearchController : Controller
    {

        private IFilmRepository FilmRepository;

        public SearchController(IFilmRepository filmRepo)
        {
            FilmRepository = filmRepo;
        }

        public ViewResult Index()
        {
            CultureInfo ci = new CultureInfo("nl-NL");
            Thread.CurrentThread.CurrentCulture = ci;

            SearchViewModel Model = new SearchViewModel();

            return View("Index",Model);
        }

        [HttpPost]
        public ViewResult Index(String CurrentSearchString, TimeSpan CurrentTimeSearch, DateTime? CurrentDateSearch)
        {
            var Films = FilmRepository.Films;

            if (!String.IsNullOrEmpty(CurrentSearchString))
            {
                Films = Films.Where(f => f.Name.ToLower().Contains(CurrentSearchString.ToLower()));
            }

            if (CurrentDateSearch >= DateTime.Today)
            {
                Films = Films.
                Join(FilmRepository.Schedules, f => f.FilmID, s => s.FilmID, (f, s) => new { Film = f, Schedule = s })
                .Where(s => s.Schedule.Date == DateTime.Now.Date)
                .GroupBy(film => film.Film.Name, (key, film) => film.FirstOrDefault())
                .Select(fg => new Film
                {
                    FilmID = fg.Film.FilmID,
                    Name = fg.Film.Name,
                    Description = fg.Film.Description,
                    Director = fg.Film.Director,
                    Language = fg.Film.Language,
                    length = fg.Film.length,
                    Type = fg.Film.Type,
                    Trailer = fg.Film.Trailer,
                    Age = fg.Film.Age,
                    SubTitles = fg.Film.SubTitles
                })
                .Distinct()
                .ToList();
            }

            SearchViewModel Model = new SearchViewModel()
            {
                Films = Films,
                CurrentSearchString = CurrentSearchString,
                CurrentDateSearch = CurrentDateSearch
            };
            return View("Result", Model);
        }
    }
}