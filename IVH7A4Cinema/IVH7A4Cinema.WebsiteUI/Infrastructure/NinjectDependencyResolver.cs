﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using IVH7A4Cinema.Domain.Abstract;
using IVH7A4Cinema.Domain.Concrete;
using System.Diagnostics.CodeAnalysis;

namespace IVH7A4Cinema.WebsiteUI.Infrastructure
{
    [ExcludeFromCodeCoverage]
    public class NinjectDependencyResolver : IDependencyResolver
    {

        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            // put bindings here
            kernel.Bind<IFilmRepository>().To<EFFilmRepository>();
            kernel.Bind<IReservationRepository>().To<EFReservationRepository>();
            kernel.Bind<IRoomRepository>().To<EFRoomRepository>();
            kernel.Bind<IPriceRepository>().To<EFPriceRepository>();
            kernel.Bind<IScheduleRepository>().To<EFScheduleRepository>();
        }
    }
}