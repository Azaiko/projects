﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IVH7A4Cinema.Domain.Abstract;
using Moq;
using IVH7A4Cinema.WebsiteUI.Controllers;
using IVH7A4Cinema.Domain.Entities;
using System.Web.Mvc;
using IVH7A4Cinema.WebsiteUI.Models;
using System.Linq;

namespace IVH7A4Cinema.WebsiteUI.Tests
{
    [TestClass]
    public class FilmControllerTest
    {
        private Mock<IFilmRepository> mockFilm;
        private FilmController controller;

        [TestInitialize]
        public void Initialize()
        {
            mockFilm = new Mock<IFilmRepository>();

            mockFilm.Setup(mfF => mfF.Films).Returns(new Film[]
            {
                new Film() { FilmID = 1, Name = "Deadpool", Description = "Na een duister experiment genezen zijn verwondingen razendsnel en bedenkt hij het alter ego Deadpool. Gewapend met zijn nieuwe gave en een goed gevoel voor (zwarte) humor gaat Deadpool op zoek naar de man die zijn leven bijna verwoestte", Director = "Tim Miller", Language = "Engels", length = 109, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=ZIM1HydF9UA&oref=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DZIM1HydF9UA", Age = "18+", SubTitles = "Nederlands" },
                new Film() { FilmID = 2, Name = "London had Fallen", Description = "Wanneer de Engelse premier onder verdachte omstandigheden dood wordt gevonden, gaan alle alarmbellen af. Zijn begrafenis blijkt de ideale plek om een aanslag te plegen en de wereld ligt plots in de handen van slechts drie personen: de president van de Verenigde Staten (Eckhart), zijn meest betrouwbare geheim agent (Butler) en een Engelse MI-6 agent (Charlotte Riley)… ", Director = "Babak Najafi", Language = "Engels", length = 99, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=3AsOdX7NcJs", Age = "16+", SubTitles = "Nederlands" },
                new Film() { FilmID = 3, Name = "The Revenant", Description = "The Revenant is een episch avontuur van één man over overleven en de buitengewone kracht van de menselijke geest. Leonardo DiCaprio heeft voor zijn rol in The Revenant de Oscar gewonnen in de categorie Beste Acteur.Iñárritu wint het beeldje in de categorie Beste Regie. Diep in de Amerikaanse wildernis wordt jager Hugh Glass(Leonardo DiCaprio) voor dood achtergelaten door John Fitzgerald(Tom Hardy), een onbetrouwbaar lid van zijn gezelschap.Met alleen zijn wil als wapen, begint Glass aan een meedogenloze missie. Hij moet zich een weg zien te banen door een levensgevaarlijke omgeving,en een strenge winter en onderling strijdende stammen overleven om wraak te kunnen nemen op Fitzgerald. Geïnspireerd op een waargebeurd verhaal.De film is geregisseerd en deels geschreven door de vermaarde filmmaker en Oscar®winnaar Alejandro González Iñárritu(Birdman, Babel).", Director = "Alejando Gonzalez Inarritu", Language = "Engels", length = 156, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=LoebZZ8K5N0", Age = "18+", SubTitles = "Nederlands" },
                new Film() { FilmID = 4, Name = "Zootropolis", Description = "Zootropolis is een stad als geen ander, een moderne wereld met alleen dieren. De stad bestaat uit verschillende wijken zoals de chique Sahara Square en het ijskoude Tundratown. Het is een mengelmoes van dieren uit verschillende omgevingen die samenleven. Het maakt niet uit wie of wat je bent, van de grootste olifant tot de kleinste spitsmuis. Maar wanneer de optimistische officier Judy Hopps (Ginnifer Goodwin) zich aansluit bij de politie, ontdekt ze dat het niet zo makkelijk is als eerste konijn bij het politiekorps vol grote, stoere dieren. Vastberaden om zichzelf te bewijzen, grijpt ze de kans aan om een zaak op te lossen. Zelfs als dat betekent dat ze moet samenwerken met de welbespraakte, sluwe vos Nick Wilde (Jason Bateman) om het mysterie op te lossen.", Director = "Byron Howard", Language = "Engels", length = 109, Type = "3D", Trailer = "https://www.youtube.com/watch?v=VCDQRy1od30", Age = "6+", SubTitles = "Nederlands" },
                new Film() { FilmID = 5, Name = "Woezel & Pip Op zoek naar de sloddervos", Description = "Woezel & Pip Op zoek naar de Sloddervos! is een vriendschapsfilm vol kleur, emotie en avontuur. Het is bijna groot feest in de Tovertuin,want de Wijze Varen wordt alweer een fraai jaar ouder.Woezel & Pip krijgen de belangrijke taak om het grote verjaardagscadeau te verstoppen, totdat de grote dag daar is. Tante Perenboom kondigt aan dat er een hele speciale gast komt logeren: het schattige neefje Charlie. Al snel ontdekken Woezel & Pip dat het cadeau en hun spullen zijn verdwenen! Wie heeft dat gedaan ? De Wijze Varen weet het antwoord, maar of de vriendjes daar zo blij mee zijn ? Woezel & Pip, Buurpoes, Molletje, Vlindertje en Charlie gaan in dit spannende avontuur op zoek naar de gevaarlijke indringer.", Director = "Rijk Aardse", Language = "Nederlands", length = 70, Type = "Normaal, Tekenfilm", Trailer = "https://www.youtube.com/watch?v=Z55IMJO2NLk", Age = "Alle leeftijden", SubTitles = "Geen" },
                new Film() { FilmID = 6, Name = "Dirty Grandpa", Description = "Over een week gaat de brave advocaat Jason (Zac Efron) trouwen met Meredith (Julianne Hough); een ontzettende control freak. Tussen alle trouwdagstress wil zijn opa (Robert De Niro) opeens een lift naar Florida. Maar daar is Spring Break in volle gang en komt het aanstaande huwelijk van Jason plots in gevaar. Want via uitzinnige feesten, bargevechten en een epische karaoke nacht is ‘vieze’ opa vast van plan zijn kleinzoon mee te sleuren in de wildste trip van hun leven!", Director = "Dan Mazer", Language = "Engels", length = 102, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=vOAn83vOZLk", Age = "16+", SubTitles = "Nederlands" },
                new Film() { FilmID = 7, Name = "Robinson Crusoe", Description = "Papegaai Dinsdag woont met zijn dierenvrienden op een paradijselijk eiland waar het ze aan niks ontbreekt. Toch droomt hij er van om de rest van de wereld te ontdekken. Na een hevige storm spoelt een merkwaardig wezen aan op het eiland: Robinson Crusoe. Dinsdag is dolblij met de nieuwkomer, wie weet is dit zijn kans om van het eiland af te komen en nieuwe plekken te ontdekken! Robinson heeft de dieren hard nodig om te overleven op het eiland en dat is in het begin nog niet makkelijk, want ze spreken geen ‘mens’.  Langzaam maar zeker leren Robinson en de dieren met elkaar te leven tot op een dag twee gemene katten het eiland willen overnemen. Er ontstaat een spannende strijd waarin Robinson en zijn vrienden zullen ontdekken hoe groot de kracht van vriendschap is.", Director = "Ben Stassen", Language = "Nederlands", length = 91, Type = "Normaal, Animatiefilm", Trailer = "https://www.youtube.com/watch?v=bo0vl4QeD-Q", Age = "6+", SubTitles = "Geen" },
                new Film() { FilmID = 8, Name = "BUURMAN & BUURMAN - AL 40 JAAR BESTE VRIENDEN!", Description = "Buurman & Buurman bestaan 40 jaar en dat mag gevierd worden! Daarom komen onze doe-het-zelf vrienden met nog nooit eerder in de bioscoop vertoonde afleveringen. Een uur vol inventief geklus met de stemmen van Kees Prins en Siem van LeeuweBuurman & Buurman – al 40 jaar beste vrienden! is een compilatie van de ongekend populaire serie over twee klunzige maar altijd positieve buurmannen.Met vijf gloednieuwe afleveringen en twee klassiekers in HD! Aan de lopende band bedenken de vrienden nieuwe klus-ideee¨n om hun levens nog leuker en handiger te maken. Het resultaat liegt er niet om: met hun eigenzinnige geklus is niets in huis veilig en de ravage blijkt enorm. Vol goede moed zien de buurmannen dit als een mooie aanleiding voor een nieuwe klus en lossen ze de mankementen samen weer op!", Director = "Marek Beneses", Language = "Nederlands", length = 56, Type = "Normaal", Trailer = "https://www.youtube.com/watch?v=CZrAp3SKD1s", Age = "3+", SubTitles = "Geen" }
            });
            this.controller = new FilmController(mockFilm.Object);
        }

        public FilmControllerTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestGetAllFilmsFromRepository()
        {
            //act
            Film[] results = ((FilmListViewModel)controller.FilmSelection().Model).Films.ToArray();
            var result = controller.FilmSelection() as ViewResult;
            //assert
            Assert.AreEqual("FilmSelection", result.ViewName);
            Assert.AreEqual("Deadpool",results[0].Name);
        }

        [TestMethod]
        public void TestGetSelectedFilm()
        {
            //act
            var result = controller.FilmDetail(3) as ViewResult;
            Film film = ((Film)controller.FilmDetail(3).Model);
            //assert
            Assert.AreEqual("FilmDetail", result.ViewName);
            Assert.AreEqual(3, film.FilmID);
        }
    }
}
