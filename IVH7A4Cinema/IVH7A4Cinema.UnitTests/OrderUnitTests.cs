﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IVH7A4Cinema.WebUI.Controllers;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Abstract;
using Moq;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.UnitTests
{
    [TestClass]
    public class OrderUnitTests
    {
        private Mock<IPriceRepository> mockPrice;
        private Mock<IFilmRepository> mockFilm;
        private Mock<IReservationRepository> mockReserv;

        [TestInitialize]
        public void Initialize()
        {
            mockPrice = new Mock<IPriceRepository>();
                mockFilm = new Mock<IFilmRepository>();
            mockReserv = new Mock<IReservationRepository>();

            mockFilm.Setup(mf => mf.Schedules).Returns(new Schedule[] {
                new Schedule { Date = DateTime.Now.Date, FilmID = 1, ScheduleID = 1, RoomID = 1, Time = DateTime.Now.TimeOfDay },
                new Schedule { Date = DateTime.Now.Date, FilmID = 2, ScheduleID = 2, RoomID = 2, Time = DateTime.Now.TimeOfDay },
                new Schedule { Date = DateTime.Now.Date, FilmID = 3, ScheduleID = 3, RoomID = 3, Time = DateTime.Now.TimeOfDay },
                new Schedule { Date = DateTime.Now.Date, FilmID = 4, ScheduleID = 4, RoomID = 4, Time = DateTime.Now.TimeOfDay }
            });
        }

        [TestMethod]
        public void TestIndexView()
        {
            //arrange S
            var controller = new HomeController();
            //act
            var result = controller.Index() as ViewResult;
            //assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void TestOrderSelectionView()
        {
            //Arrange


            Schedule schedule = new Schedule { Date = DateTime.Now.Date, FilmID = 1, RoomID = 1, Time = DateTime.Now.TimeOfDay };
            
            OrderController controller = new OrderController(mockPrice.Object, mockFilm.Object, mockReserv.Object);
            //Act
            var result = controller.OrderSelection(schedule) as ViewResult;
            //Assert
            Assert.AreEqual("OrderSelection", result.ViewName);
        }

        //[TestMethod]
        //public void testOrderSelectionTotalSeats()
        //{
        //    //Arrange
        //    var form = CreateTicketsTestFormCollection();
        //    //Act
        //    var result = ;
        //    //Assert
        //    Assert.AreEqual("RenderRoomLayout", form);
        //}

        private static FormCollection CreateTicketsTestFormCollection()
        {
            FormCollection form = new FormCollection();
            form.Add("textFieldNormal", "2");
            form.Add("textFieldChild", "2");
            form.Add("textFieldStudent", "2");
            form.Add("textFieldSenior", "2");

            return form;
        }

        [TestMethod]
        public void testRenderRoomLayoutView()
        {
            //arrange
            Mock<IPriceRepository> mockPrice = new Mock<IPriceRepository>();
            Mock<IFilmRepository> mockFilm = new Mock<IFilmRepository>();

            FormCollection form = new FormCollection();
            form = CreateTicketsTestFormCollection();

            mockFilm.Setup(mf => mf.Schedules).Returns(new Schedule[] {
                new Schedule { Date = DateTime.Now.Date, FilmID = 1, ScheduleID = 1, RoomID = 1, Time = DateTime.Now.TimeOfDay },
                new Schedule { Date = DateTime.Now.Date, FilmID = 2, ScheduleID = 2, RoomID = 2, Time = DateTime.Now.TimeOfDay },
                new Schedule { Date = DateTime.Now.Date, FilmID = 3, ScheduleID = 3, RoomID = 3, Time = DateTime.Now.TimeOfDay },
                new Schedule { Date = DateTime.Now.Date, FilmID = 4, ScheduleID = 4, RoomID = 4, Time = DateTime.Now.TimeOfDay }
            });

            var controller = new OrderController(mockPrice.Object, mockFilm.Object, mockReserv.Object);
            //act
            var result = controller.OrderSelection(form) as RedirectToRouteResult;
            //assert
            Assert.AreEqual("RenderRoomLayout", result.RouteValues["action"]);
            Assert.AreEqual("Room", result.RouteValues["controller"]);
        }
    }
}
