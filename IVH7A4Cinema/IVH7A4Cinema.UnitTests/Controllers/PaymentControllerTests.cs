﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IVH7A4Cinema.WebUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using IVH7A4Cinema.Domain.Abstract;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Entities;


namespace IVH7A4Cinema.WebUI.Controllers.Tests
{
    [TestClass()]
    public class PaymentControllerTests
    {
        [TestMethod()]
        public void TestPaymentCheck()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            // Arrange
            var controller = new PaymentController(reservationMock.Object);
            // Act
            Reservation r = new Reservation { ReservationID = 2, CustomerID = 4, Paid = false, Printed = false };
            var result = controller.PaymentCheck(2) as ViewResult;
            // Arrange
            Assert.AreEqual("PaymentCheck", result.ViewName);
        }
    }
}