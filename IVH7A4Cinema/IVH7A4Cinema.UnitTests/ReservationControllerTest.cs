﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IVH7A4Cinema.WebUI.Controllers;
using Moq;
using IVH7A4Cinema.Domain.Abstract;
using System.Web.Mvc;
using IVH7A4Cinema.Domain.Entities;

namespace IVH7A4Cinema.UnitTests
{
    [TestClass]
    public class ReservationControllerTest
    {
        [TestMethod]
        public void TestReservationHome()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            // Arrange
            var controller = new ReservationController(reservationMock.Object);
            // Act
            var result = controller.ReservationHome() as ViewResult;
            // Arrange
            Assert.AreEqual("ReservationHome", result.ViewName);
        }

        [TestMethod]
        public void ReservationCheckNotValid()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            reservationMock.Setup(r => r.Reservations).Returns(new Reservation[]
            {
                new Reservation {ReservationID = 2, CustomerID = 4, Paid = false, Printed = false },
                new Reservation {ReservationID = 3, CustomerID = 3, Paid = true, Printed = false },
                new Reservation {ReservationID = 4, CustomerID = 5, Paid = true, Printed = true }
            });
            // Arrange

            var controller = new ReservationController(reservationMock.Object);

            // Act
            ActionResult result = controller.ReservationCheck(new Reservation { ReservationID = 1, CustomerID = 2 });

            // Assert
            //Assert.AreEqual("ReservationHome", result.ViewName);
            //Assert.AreEqual("De ingevoerde ID is ongeldig, raadpleeg een medewerker als u het ID correct heeft ingevoerd.", result.ViewBag.Error);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


        [TestMethod]
        public void ReservationCheckValid()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            reservationMock.Setup(r => r.Reservations).Returns(new Reservation[]
            {
                new Reservation {ReservationID = 2, CustomerID = 4, Paid = false, Printed = false },
                new Reservation {ReservationID = 3, CustomerID = 3, Paid = true, Printed = false },
                new Reservation {ReservationID = 4, CustomerID = 5, Paid = true, Printed = true }
            });
            // Arrange
            var controller = new ReservationController(reservationMock.Object);

            // Act
            var result = controller.ReservationCheck(new Reservation { ReservationID = 2, CustomerID = 2 }) as RedirectToRouteResult;

            // Assert
            Assert.AreEqual("PaymentCheck", result.RouteValues["action"]);
            Assert.AreEqual("Payment", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void ReservationCheckPaid()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            reservationMock.Setup(r => r.Reservations).Returns(new Reservation[]
            {
                new Reservation {ReservationID = 2, CustomerID = 4, Paid = false, Printed = false },
                new Reservation {ReservationID = 3, CustomerID = 3, Paid = true, Printed = false },
                new Reservation {ReservationID = 4, CustomerID = 5, Paid = true, Printed = true }
            });
            // Arrange
            var controller = new ReservationController(reservationMock.Object);

            // Act
            var result = controller.ReservationCheck(new Reservation { ReservationID = 3, CustomerID = 2 }) as RedirectToRouteResult;

            // Assert
            Assert.AreEqual("TicketOverview", result.RouteValues["action"]);
            Assert.AreEqual("Ticket", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void ReservationCheckPaidPrinted()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            reservationMock.Setup(r => r.Reservations).Returns(new Reservation[]
            {
                new Reservation {ReservationID = 2, CustomerID = 4, Paid = false, Printed = false, RowNumber = 1, SeatNumber = 2 },
                new Reservation {ReservationID = 3, CustomerID = 3, Paid = true, Printed = false, RowNumber = 1, SeatNumber = 3 },
                new Reservation {ReservationID = 4, CustomerID = 5, Paid = true, Printed = true, RowNumber = 1, SeatNumber = 4 }
            });
            // Arrange
            var controller = new ReservationController(reservationMock.Object);

            // Act
            var result = controller.ReservationCheck(new Reservation { ReservationID = 4, CustomerID = 2 }) as RedirectToRouteResult;

            // Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestReservationIDCheckTrue()
        {
            Mock<IReservationRepository> reservationMock = new Mock<IReservationRepository>();
            reservationMock.Setup(r => r.Reservations).Returns(new Reservation[]
            {
                new Reservation {ReservationID = 2, CustomerID = 4, Paid = false, Printed = false },
                new Reservation {ReservationID = 3, CustomerID = 3, Paid = true, Printed = false },
                new Reservation {ReservationID = 4, CustomerID = 5, Paid = true, Printed = true }
            });

            // Arrange
            var controller = new ReservationController(reservationMock.Object);

            var result = controller.ReservationIDCheck(2);
            //asert
            Assert.AreEqual(true, result);
        }
    }
}
