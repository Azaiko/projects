﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Birthday.Models
{
    public class BirthdayModel {
        [Required(ErrorMessage = "Voer alsjeblieft je naam in")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Voer alsjeblieft je geboortedatum dag in")]
        [RegularExpression(@"^(([1-9])|(1[0-9])|(2[0-9])|(3[0-1]))$", ErrorMessage = "Voer alsjeblieft een dag tussen 1 en 31 in")]
        public int Day { get; set; }
        [Required(ErrorMessage = "Voer alsjeblieft je geboortedatum maand in")]
        [RegularExpression(@"^(([1-9])|(0[1-9])|(1[0-2]))$", ErrorMessage = "Voer alsjeblieft een maand tussen 1 en 12 in")]
        public int Month { get; set; }
        [Required(ErrorMessage = "Voer alsjeblieft je geboortedatum jaar in")]
        [RegularExpression(@"^((1[9][0-9][0-9])|(2[0][0-9][0-9]))$", ErrorMessage = "Voer alsjeblieft een jaar in tussen 1900 en 2099")]
        public int Year { get; set; }
    }
}