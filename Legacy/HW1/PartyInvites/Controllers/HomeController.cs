﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Birthday.Models;

namespace Birthday.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            int day = DateTime.Now.Day;
            string month = DateTime.Now.ToString("MMMM");
            int year = DateTime.Now.Year;
            ViewBag.Date = day + " "+ month + " " + year;
            return View();
        }
        
        [HttpGet]
        public ViewResult BirthdayDataForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult BirthdayDataForm(BirthdayModel birthday)
        {
            if (ModelState.IsValid)
            {
                DateTime dateNow = DateTime.Now;
                DateTime dateUser = new DateTime(birthday.Year, birthday.Month, birthday.Day);
                ViewBag.Name = birthday.Name;
                if (birthday.Day == DateTime.Now.Day && birthday.Month == DateTime.Now.Month)
                {
                    ViewBag.Age = GetAge(dateUser);
                    return View("HappyBirthday", birthday);
                }
                else
                {
                    ViewBag.DaysUntilBirthday = GetDaysUntilBirthday(dateUser);
                    ViewBag.Age = GetAge(dateUser) + 1;
                    return View("DaysUntilBirthday", birthday);
                }
            }
            else {
                return View();
            }
        }

        private static int GetDaysUntilBirthday(DateTime birthday)
        {
            var nextBirthday = birthday.AddYears(DateTime.Today.Year - birthday.Year);
            if (nextBirthday < DateTime.Today)
            {
                nextBirthday = nextBirthday.AddYears(1);
            }
            return (nextBirthday - DateTime.Today).Days;
        }

        private static int GetAge(DateTime birthday)
        {
            var nextBirthday = birthday.Date;
            DateTime today = DateTime.Today;
            int age = today.Year - nextBirthday.Year;
            if (nextBirthday > today.AddYears(-age)) age--;
            return age;
        }

    }
}