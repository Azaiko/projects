﻿using QuizApp.Models;
using QuizApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizApp.Controllers
{
    public class QuizController : Controller
    {
        private QuizService quizService;

        public QuizController()
        {
            quizService = new QuizService();
        }

        [HttpGet]
        public ActionResult Publish()
        {
            var quiz = quizService.InitiateQuiz();
            return View(quiz);
        }

        public ActionResult Grade(Quiz quiz)
        {
            var grade = quizService.Grade(quiz);
            return View(grade);
        }
    }
}