﻿using QuizApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizApp.Services
{
    public class QuizService
    {
        private bool shuffle;

        public Quiz InitiateQuiz()
        {
            shuffle = true;
            var quiz = new Quiz() { Id = 100, Name = "Warcraft universe lore quiz" };
            quiz.AddQuestion(GetQuestions());
            HttpContext.Current.Session["Quiz"] = quiz;
            return quiz;
        }

        public Quiz GetQuiz()
        {
            Quiz quiz = (Quiz)HttpContext.Current.Session["Quiz"];
            return quiz;
        }

        private IList<Question> GetQuestions()
        {

            Random rng = new Random();

            var questions = new List<Question>() {
                new Question() { Text = "Which warlock is considered to be the most powerful, mortal Warlock to have ever existed?", Point = 1, Id = 1, OrderNumber = 0 },
                new Question() { Text = "Who is the leader of the Dragon Aspects?", Point = 1, Id = 2, OrderNumber = 1 },
                new Question() { Text = "Who was the last Guardian of Tirisfal?", Point = 1, Id = 3, OrderNumber = 2 },
                new Question() { Text = "Which of these clans did not join Thrall and his new Horde?", Point = 1, Id = 4, OrderNumber = 3 },
                new Question() { Text = "The last Guardian chooses to shapeshift into?", Point = 1, Id = 5, OrderNumber = 4 },
                new Question() { Text = "What was the name of Thrall his father?", Point = 1, Id = 6, OrderNumber = 5 },
                new Question() { Text = "Which titan became corrupted by the Burning Legion?", Point = 1, Id = 7, OrderNumber = 6 },
                new Question() { Text = "Who was a former Lich King?", Point = 1, Id = 8, OrderNumber = 7 },
                new Question() { Text = "Long ago, the pandaren were slaves of a cruel race of powerful warlords known as ...?", Point = 1, Id = 9, OrderNumber = 8 },
                new Question() { Text = "What was the name of Cairne Bloodhoof his sun?", Point = 1, Id = 10, OrderNumber = 9 },
                new Question() { Text = "What did the so called 'Northrend campaign' hope to achieve?", Point = 1, Id = 11, OrderNumber = 10 }
                };

            questions[0].AddChoice(new Choice() { IsAnswer = false, Text = "Cho'gall", Id = 1 });
            questions[0].AddChoice(new Choice() { IsAnswer = true, Text = "Gul'dan", Id = 2 });
            questions[0].AddChoice(new Choice() { IsAnswer = false, Text = "Kil'jaeden", Id = 1 });
            questions[0].AddChoice(new Choice() { IsAnswer = false, Text = "Sargeras", Id = 1 });
            questions[0].AddChoice(new Choice() { IsAnswer = false, Text = "Ner'zhul", Id = 1 });

            questions[1].AddChoice(new Choice() { IsAnswer = false, Text = "Nozdormu the Timeless One", Id = 3 });
            questions[1].AddChoice(new Choice() { IsAnswer = true, Text = "Alexstrasza the Life-Binder", Id = 4 });
            questions[1].AddChoice(new Choice() { IsAnswer = false, Text = "Ysera the Dreamer", Id = 3 });
            questions[1].AddChoice(new Choice() { IsAnswer = false, Text = "Malygos the Spell-Weaver", Id = 3 });
            questions[1].AddChoice(new Choice() { IsAnswer = false, Text = "Neltharion the Earth-Warder", Id = 3 });

            questions[2].AddChoice(new Choice() { IsAnswer = false, Text = "Alodi", Id = 6 });
            questions[2].AddChoice(new Choice() { IsAnswer = false, Text = "Scavell", Id = 6 });
            questions[2].AddChoice(new Choice() { IsAnswer = false, Text = "Med'an", Id = 6 });
            questions[2].AddChoice(new Choice() { IsAnswer = false, Text = "Aegwynn", Id = 6 });
            questions[2].AddChoice(new Choice() { IsAnswer = true, Text = "Medivh", Id = 5 });

            questions[3].AddChoice(new Choice() { IsAnswer = false, Text = "Frostwolf clan", Id = 7 });
            questions[3].AddChoice(new Choice() { IsAnswer = false, Text = "Warsong clan", Id = 7 });
            questions[3].AddChoice(new Choice() { IsAnswer = false, Text = "Shadowmoon clan", Id = 7 });
            questions[3].AddChoice(new Choice() { IsAnswer = false, Text = "Stormreaver clan", Id = 7 });
            questions[3].AddChoice(new Choice() { IsAnswer = true, Text = "Burning blade clan", Id = 8 });
        
            questions[4].AddChoice(new Choice() { IsAnswer = false, Text = "Viper", Id = 9 });
            questions[4].AddChoice(new Choice() { IsAnswer = false, Text = "Hawk", Id = 9 });
            questions[4].AddChoice(new Choice() { IsAnswer = true, Text = "Raven", Id = 10 });
            questions[4].AddChoice(new Choice() { IsAnswer = false, Text = "Eagle", Id = 9 });
            questions[4].AddChoice(new Choice() { IsAnswer = false, Text = "Wisp", Id = 9 });

            questions[5].AddChoice(new Choice() { IsAnswer = false, Text = "Gazlowe", Id = 12 });
            questions[5].AddChoice(new Choice() { IsAnswer = false, Text = "Doomhammer", Id = 12 });
            questions[5].AddChoice(new Choice() { IsAnswer = false, Text = "Khadgar", Id = 12 });
            questions[5].AddChoice(new Choice() { IsAnswer = true, Text = "Durotan", Id = 11 });
            questions[5].AddChoice(new Choice() { IsAnswer = false, Text = "Gul'dan", Id = 12 });

            questions[6].AddChoice(new Choice() { IsAnswer = false, Text = "Eonar", Id = 14 });
            questions[6].AddChoice(new Choice() { IsAnswer = false, Text = "Aggramar", Id = 14 });
            questions[6].AddChoice(new Choice() { IsAnswer = false, Text = "Golganneth", Id = 14 });
            questions[6].AddChoice(new Choice() { IsAnswer = false, Text = "Khaz'goroth", Id = 14 });
            questions[6].AddChoice(new Choice() { IsAnswer = true, Text = "Sargeras", Id = 13 });

            questions[7].AddChoice(new Choice() { IsAnswer = true, Text = "Arthas Menethil", Id = 15 });
            questions[7].AddChoice(new Choice() { IsAnswer = false, Text = "Kil'jaeden", Id = 16 });
            questions[7].AddChoice(new Choice() { IsAnswer = false, Text = "Uther Lightbringer", Id = 16 });
            questions[7].AddChoice(new Choice() { IsAnswer = false, Text = "Tirion Fordring", Id = 16 });
            questions[7].AddChoice(new Choice() { IsAnswer = false, Text = "Varian Wrynn", Id = 16 });

            questions[8].AddChoice(new Choice() { IsAnswer = false, Text = "Sha", Id = 17 });
            questions[8].AddChoice(new Choice() { IsAnswer = false, Text = "Grummles", Id = 17 });
            questions[8].AddChoice(new Choice() { IsAnswer = false, Text = "Zalandar trolls", Id = 17 });
            questions[8].AddChoice(new Choice() { IsAnswer = false, Text = "Mantid", Id = 17 });
            questions[8].AddChoice(new Choice() { IsAnswer = true, Text = "Mogu", Id = 18 });

            questions[9].AddChoice(new Choice() { IsAnswer = false, Text = "Magatha", Id = 19 });
            questions[9].AddChoice(new Choice() { IsAnswer = false, Text = "Bovan", Id = 19 });
            questions[9].AddChoice(new Choice() { IsAnswer = true, Text = "Baine", Id = 20 });
            questions[9].AddChoice(new Choice() { IsAnswer = false, Text = "Trag", Id = 19 });
            questions[9].AddChoice(new Choice() { IsAnswer = false, Text = "Aponi", Id = 19 });

            questions[10].AddChoice(new Choice() { IsAnswer = false, Text = "To find new furtile lands", Id = 21 });
            questions[10].AddChoice(new Choice() { IsAnswer = false, Text = "To sacrifice Bolvar Fordragon", Id = 21 });
            questions[10].AddChoice(new Choice() { IsAnswer = false, Text = "To start a war against the Alliance", Id = 21 });
            questions[10].AddChoice(new Choice() { IsAnswer = true, Text = "To defeat the Lich King", Id = 22 });
            questions[10].AddChoice(new Choice() { IsAnswer = false, Text = "To held the Argent Tournament", Id = 21 });

            if (shuffle == true) { 
                int n = questions.Count;
                while (n > 1)
                {
                    n--;
                    int k = rng.Next(n + 1);
                    var value = questions[k];
                    questions[k] = questions[n];
                    questions[n] = value;
                }
            }

            return questions.GetRange(0,5);
        }

        public Grade Grade(Quiz toBeGradedQuiz)
        {
            var persistedQuiz = GetQuiz();
            var grade = new Grade() { Quiz = persistedQuiz };

            foreach (var question in toBeGradedQuiz.Questions)
            {
                var persistedQuestion = (from q in persistedQuiz.Questions
                                         where q.Id == question.Id
                                         select q).FirstOrDefault();

                if (persistedQuestion != null)
                {
                    foreach (var choice in question.Choices)
                    {
                        var persistedChoice = (from c in persistedQuestion.Choices
                                               where c.Id == choice.Id
                                               select c).FirstOrDefault();

                        persistedChoice.IsSelected = true;

                        if (persistedChoice.IsAnswer)
                        {
                            grade.Score += persistedQuestion.Point;
                        }
                    }
                }
            }

            grade.Score = (grade.Score / persistedQuiz.Questions.Count * 100);

            return grade;
        }
    }            

}