﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizApp.Models
{
    public class Grade
    {
        public double Score { get; set; }
        public Quiz Quiz { get; set; }
    }
}
