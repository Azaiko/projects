﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizApp.Models
{
    public class Choice
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsAnswer { get; set; }
        private Question question = new Question();
        public bool IsSelected { get; set; }

        public Question Question
        {
            get { return question; }
            set { question = value; }
        }
    }
}