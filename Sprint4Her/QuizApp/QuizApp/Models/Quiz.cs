﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizApp.Models
{
    public class Quiz
    {
        public int Id { get; set; }
        public IList<Question> questions = new List<Question>();
        public string Name { get; set; }

        public IList<Question> Questions
        {
            get { return questions; }
            set { questions = value; }
        }

        public void AddQuestion(IList<Question> questions)
        {
            foreach (var question in questions)
            {
                AddQuestion(question);
            }
        }

        public void AddQuestion(Question question)
        {
            questions.Add(question);
        }
    }
}