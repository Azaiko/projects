namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeddialogue : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dialogues", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Dialogues", new[] { "UserId" });
            DropTable("dbo.Dialogues");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Dialogues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Dialogues", "UserId");
            AddForeignKey("dbo.Dialogues", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
